 # Manuel Utilisateur

# Introduction

Ce manuel s’adresse aux personnes qui veulent compiler leurs programmes avec le compilateur que notre hexanome (H4242 2020-2021) a élaboré au cours du PLD-COMP.

# Commandes

Afin d'utiliser le compilateur, il faut utiliser la commande suivante: <br>
`./ifcc [fichier.c] [option]` <br>
Par défaut, le compilateur générera de l'assembleur en x86. <br>
Pour générer en ARM, il faut utiliser `arm` ou `ARM` en `[option]`. <br>
Attention: la compilation en assembleur ARM est incomplète et non formellement testée.
 
# Langage compilable

Le language qui peut etre compilé est un sous-ensemble du langage C. 
Les spécifications sont les suivantes: 

## 1. Les Types 
 
Les types implémentés sont : 

- Le type `int` : correspond aux nombres entiers.
- Le type `void` : correspond au type vide, il existe uniquement pour le type de retour des fonctions.


## 2. Structure générale du code

Un programme interprété par notre compilateur a la structure suivante:

```
Directive*

Fonctions*

main
```

Nous allons détailler ci-dessous ces différentes parties de la structure du code.

---
### 2.1 Directives : les include 
---

Notre compilateur lit les include. Ces derniers ne sont néanmoins pas interprétés. Cela signifie que si un programme c contient un include, cela ne sera pas bloquant pour le compilateur.

_Exemple de code :_

```
#include "math.h"
```

--- 
### 2.2 Les fonctions
---

Notre compilateur lit les fonctions et les interprête tel que nous l'expliquons plus bas.

_Exemple de code :_

```
type fonction(args) {
   instructions
   return valeur;
}

int main()
{
    int argsCall;
    int ret = fonction(argsCall);
    return ret;
}

```

---
#### 2.2.1 Déclaration de fonction
---

La déclaration des fonctions se fait à la suite des include. Elles prennent, comme en C, la forme suivante : 
 
```
type fonction(args) {
   instructions
   return expression;
}
```
 
Dans cette structure générique, on retrouve différents éléments : 
- `type` correspond au type de variable que la fonction va renvoyer. Il peut valoir `void` ou `int` dans notre implémentation.
- `fonction` correspond au label de la fonction. On peut nommer les fonctions de la même manière que dans le langage C classique.
- `args` correspond aux arguments passés à la fonction. Ils sont déclarés de la même manière que dans C. La seule nuance est qu'il ne peuvent avoir que le type `int` car c'est le seul que nous prenons en charge.
- `instructions` correspond au code qui va être executé lors de l’appel de la fonction. Nous détaillerons ce qu'il peut contenir plus loin.
- `return` précède l'expression' que la fonction va retourner.
- `expression` est l'expression que la fonction va interprêter, puis retourner. Son type doit correspondre à celui déclaré avant le nom de la fonction. 


_Exemple de code :_

```
int addition(int a, int b) {
   int c = a + b ;
   return c;
}
```
 
---
#### 2.2.2 Appel de fonction
---

L'appel d'une fonction se fait dans les instructions du main ou d'une autre fonction. Elle prend la forme suivante: 

```
fonction(argsCall)
```

- `fonction` correspond au label d'une fonction déclarée précédement dans le code.
- `args` correspond aux arguments passés à la fonction. Leur nombre et leur type doit correspondre à ceux déclarés dans la définition de la fonction.

 _Exemple de code :_

```
int addition(int a, int b) 
{
   int c = a + b ;
   return c;
}

int main()
{
    int b = 2;
    int c = addition(3,b);
    return c;
}

```

---
### 2.3 La fonction main 
---
 
La fonction main se trouve à la fin du programme, c’est elle qui est exécutée à l’execution du programme. 

Elle prend la même forme qu'une fonction classique à la seule différence que son label est : `valeur` 


_Exemple de code :_
```
int main()
{
    int b = 2;
    int c = 3 + b;
    return c;
}
```

--- 
## 3. Déclaration, définition, affectation
---

Les déclarations, définitions, affectations s'appliquent à des variables.

_Exemple de code :_
```
int dec;
dec = 3;
int def = dec;
```

---
### 3.1 Déclaration
---

La déclaration permet d'assigner une case de mémoire à une variable. Elle permet de créer une variable et de lui assigner une case mémoire.

```
type noms ;
```

- `type` correspond au type de la variable déclarée, qui ne peut être que `int` pour notre compilateur.
- `noms` correspond aux noms des variables. C'est-à-dire les noms avec lequels on pourra leur faire référence.  
 
 
 _Exemple de code :_

```
int nb_pomme, nb_poire, nb_kiwi ;
```


--- 
### 3.2 Affectation
---

L'affectation permet d'assigner une valeur à une variable (qui doit avoir été déclarée préalablement).

```
nom = valeur;
```

- `nom` correspond au nom de la variable. C'est-à-dire le nom avec lequel on pourra lui faire référence.  

- `valeur` correspond à la valeur que l'on va assigner à la variable. Elle peut être une constante, une variable ou une expression (concept qui sera décrit plus bas). Elle doit avoir le même type que la variable à laquelle est affectée.  


 _Exemple de code :_

```
nb_pomme = 3;
```


---
### 3.3 Définition 
--- 
 
Le définiton consiste en la création d'une variable et en l'écriture d'une valeur dans sa case mémoire.

```
int nom = valeur;
```

- `type` correspond au type de la variable déclarée, qui ne peut être que `int` pour notre compilateur.
- `nom` correspond au nom de la variable. C'est-à-dire le nom avec lequel on pourra lui faire référence.  
- `valeur` correspond à la valeur que l'on va assigner à la variable. Elle peut être une constante ou une variable. Elle doit avoir le même type que la variable à laquelle elle est affectée.   

 _Exemple de code :_

```
int nb_pomme = 3;
```

---
## 4. Les Expressions
---


Une expression est un ensemble constitué de variables, constantes et appels de fonctions liés avec des opérateurs. Chaque expression a une valeur et un type. Dans l'exemple suivant, l'expression est constituée uniquement de la constante 3.

```
int expression = 3;
```

---
### 4.1 Les expressions primaires
---


Les expressions primaires implémentées sont : 

- Les constantes : nombres entiers constants.

- Les variables : le nom d'une variable commençant nécessairement par une lettre, elle peut ensuite être composée de lettres, de chiffres et de "_".

 _Exemple de code :_
```
int expression1 = 3;
int expression2 = variable;
```
---
### 4.2 Appels de fonctions
---

Une expression peut aussi prendre la forme d'un appel à une fonction. Sa valeur correspond alors à la valeur renvoyée par la fonction.

 _Exemple de code :_
```
int expression = fonctionDefinie(a,b,z);
```
---
### 4.3 Les opérateurs
---

Les opérateurs implémentés peuvent être appliqués à toute expression. Les opérateurs unaires ne prennent qu'un argument, et les autres opérateurs prennent deux arguments.

---
#### 4.3.1 Les opérateurs unaires 
---

Les opérateurs unaires sont implémentés sous la forme suivante :

```
Op Expr1
``` 

Les opérateurs unaires implémentés sont : 

- L’opposé `-` : renvoie l'opposé de `Expr1`
- La négation `!` : renvoie la négation (bit à bit) de `Expr1`

_Exemple de code :_
```
int main()
{
    int a = 2;     
    int opp = -a;
    int neg = !a;
    return a;
}
```

---
#### 4.3.2 Les opérateurs arithmétiques
---

Les opérateurs arithmétiques sont implémentés sous la forme suivante :
 
```
Expr1 Op Expr2
```

Les opérateurs arithmétiques implémentés sont les suivants : 

- L’addition `+` : renvoie le résultat de l'addition entre `Expr1` et `Expr2`.
- La multiplication `*` : renvoie le résultat de la multiplication entre `Expr1` et `Expr2`.
- La soustraction `-` : renvoie le résultat de `Expr1` auquel on soustrait Expr2 .
- La division `/` : renvoie le résultat de la division euclidienne de `Expr1` par Expr2.
- Le modulo `%` : renvoie le reste de la division euclidienne de `Expr1` par Expr2.

_Exemple de code :_

```
int main()
{
    int a = 2;     
    int b = 3 + a; 
    int c = b - 4; 
    int d = 5 * c; 
    int e = 6 / d; 
    int f = 7 % e; 
    return f;
}
```

---
#### 4.3.3 Les opérateurs relationnels
---

Les opérateurs relationnels sont implémentés sous la forme suivante :

```
Expr1 Op Expr2
```

Les opérateurs relationnels implémentés sont :

- L'opérateur supérieur `>` : renvoie 1 si `Expr1` est supérieur à `Expr2`, et 0 sinon.
- L'opérateur inférieur `<` : renvoie 1 si Expr1 est inférieur à Expr2, et 0 sinon.
- L'opérateur supérieur ou égal `>=` : renvoie 1 si `Expr1` est supérieur ou égal à `Expr2`, et 0 sinon.
- L'opérateur inférieur ou égal `<=` : renvoie 1 si `Expr1` est inférieur ou égal à `Expr2`, et 0 sinon.
- L'opérateur d'égalité `==` : renvoie 1 si `Expr1` est égal à `Expr2`, et 0 sinon.
- L'opérateur de non-égalité `!=` : renvoie 1 si `Expr1` n'est pas égal à `Expr2`, et 0 sinon.


_Exemple de code :_
```
int main()
{
    int a = 2;     
    int sup = (a>3);
    int inf = (a<3);
    int supOrEq = (a>=3);
    int infOrEq = (a<=3);
    int eq = (a==3);
    int notEq = (a!=3);
    return a;
}
```
---
#### 4.3.4 Les opérateurs bit à bit
---

Les opérateurs bit à bit sont implémentés sous la forme suivante :

```
Expr1 Op Expr2
``` 

Les opérateurs bit à bit implémentés sont :

- Le et `&` : renvoie le nombre dont les bits sont ceux de `Expr1` et `Expr2` comparés un à un, si les deux valent 1 alors le bit du nombre renvoyé vaut 1 . 

- Le ou `|` : renvoie le nombre dont les bits sont ceux de `Expr1` et `Expr2` comparés un à un, si au moins un des deux vaut 1 alors le bit du nombre renvoyé vaut 1.

- Le ou exclusif `^` : renvoie le nombre dont les bits sont ceux de `Expr1` et `Expr2` comparés un à un, si pile un des deux vaut 1 alors le bit du nombre renvoyé vaut 1.


_Exemple de code :_
```
int main()
{
    int a = 2;     
    int et = (a&1);
    int ou = (a|1);
    int ouX = (a^1);
    return a;
}
```

---
#### 4.3.5 Les opérateurs logiques 
---

Les opérateurs bit à bit sont implémentés sous la forme suivante :

```
Expr1 Op Expr2
``` 

Les opérateurs logiques implémentés sont : 

- Le et `&&` : renvoie 1 si `Expr1` et `Expr2` vallent tout entier différent de 0, et 0 sinon.
- Le ou `||` : renvoie 1 si `Expr1` et `Expr2` vallent 0, et 1 sinon.


_Exemple de code :_
```
int main()
{
    int a = 2;     
    int et = (a&&1);
    int ou = (a||1);
    return a;
}
```

---
## 5. Conditions, Boucles
---

### 5.1 If … Else
---

Notre compilateur gère les conditions du type `if()...else`. Ils suivent la structure suivante :


```
if(Condition)
{
    instructions1 
}
else
{
    instructions2 
}
 ```


- `instructions1` et `instructions2` correspondent à des suites d'instructions.
- `Condition` consiste en une expression qui prend une valeur différente de 0 si l'on doit exécuter `instructions1` ou 0 si l'on doit exécuter `instructions2`.




_Exemple de code :_
```
int main()
{
    int a = 2;     
    int b = 3;
    int c;
    
    if(a > b)
    {
        c = a; 
    }
    else
    {
        c = b;
    }
    return c;
}
```

---
### 5.2 While
---
Notre compilateur gère les boucles conditionnelles du type `while()`. Voici le schéma qu'elles doivent suivre :


```
while(condition)
{
    instructions
}
 ```
 
- `instructions` est une suite d'instructions.
- `Condition` consiste en une expression. Le programme exécute `instructions` tant que la valeur de l'expression est différente de 0.



_Exemple de code :_
```
int main()
{
    int a = 20;     
    int b = 3;
    int c = 0;
    
    while(a > b)
    {
        a = a-1;
        c = c + 1 ;
    }
    return c;
}
```

 



