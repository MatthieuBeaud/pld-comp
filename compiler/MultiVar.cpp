#include "MultiVar.h"
#include <iostream>

MultiVar::MultiVar()
{
    nomsVars = list<string>();
}

MultiVar::~MultiVar()
{

}

void MultiVar::DisplayVars()
{
    for(list<string>::iterator it = nomsVars.begin(); it!=nomsVars.end(); it++)
        {
            cout<<*it<<endl;
        }
}

void MultiVar::AddToTableSymbole(CFG& cfg, Type t)
{
    for(list<string>::iterator it = nomsVars.begin(); it!=nomsVars.end(); it++)
    {
        cfg.add_to_symbol_table(*it, t);
    }
}