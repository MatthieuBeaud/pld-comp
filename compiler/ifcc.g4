grammar ifcc;

axiom : prog       
      ;

prog : directive* function* main;


main : 'int' 'main' '(' ')' '{' code '}' ;

function: functionType VARIABLE '(' params ')' '{' code '}' #declarationAvecParams
            | functionType VARIABLE '('  ')' '{' code '}' #declarationSansParams
            ;

functioncall : VARIABLE '(' paramscall ')'  #AppelAvecParams
            | VARIABLE '(' ')'  #AppelSansParams
            ;

code:   declaration code
      | definition code
      | affectation code
      | functioncall ';' code
      | condition code
      | boucleWhile code
      | (returnStatement code)* 
      ;

returnStatement: RETURN ';'         #returnVoid
            | RETURN expression ';' #returnExpr
            ; 

directive: DIRECTIVE
	   | DIRECTIVE directive;

declaration: TYPE multivar ';' ;

definition : TYPE VARIABLE '=' expression ';';

affectation : VARIABLE '=' expression ';';

expression : val                      #valeur
          | '!' expression #not
          | '-' expression #opp
          | expression '*' expression #mult
          | expression '/' expression #div
          | expression '%' expression #mod
          | expression '+' expression #add
          | expression '-' expression #sub
          | expression '&' expression #and
          | expression '&&' expression #and
          | expression '||' expression #or
          | expression '|' expression #or
          | expression '^' expression #xor
          | expression '==' expression #equal
          | expression '!=' expression #nequal
          | expression '>' expression #greater
          | expression '<' expression #less
          | expression '>=' expression #greaterequal
          | expression '<=' expression #lessequal
          | functioncall             #call
          | '(' expression ')'        #par   
          ;

boucleWhile: 'while' '(' expression ')' '{' code '}';
condition: 'if' '(' expression ')' '{' code '}'
         | 'if' '(' expression ')' '{' code '}' alternative
         ;
alternative: 'else' '{' code '}';

val: VARIABLE | CONST ;
multivar: VARIABLE | multivar ',' VARIABLE ;



params: param | params ',' param;
param: TYPE VARIABLE;

paramscall: expression | paramscall ',' expression ;

functionType : TYPE | 'void';

TYPE : 'int';
RETURN : 'return' ;
VARIABLE : [a-zA-Z_][a-z0-9_A-Z]* ;
CONST : [0-9]+ ;
COMMENT : '/*' .*? '*/' -> skip ;
DIRECTIVE : '#' .*? '\n' -> skip ;
WS    : [ \t\r\n] -> channel(HIDDEN);
