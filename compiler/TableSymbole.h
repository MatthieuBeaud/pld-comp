#pragma once

#include<map>
#include<vector>
#include<string>

using namespace std;

enum Type{ INT, CHAR };
enum ReturnType{ R_INT, R_CHAR, R_VOID };

typedef struct {
    Type type;
    int offset;
    bool used;
} Variable;

typedef struct {
    Type type;
    string  nom;
} Parametre;

typedef struct {
    ReturnType returnType;
    vector<Type> params;
} Function;

class TableSymbole
{
    
    public:
        TableSymbole();
        TableSymbole(const TableSymbole  & tableCopie) ;
        TableSymbole& operator=(TableSymbole const& tableCopie); // copy assignment 

        ~TableSymbole();
        void AjouterSymbole(string nom, Type type);
        string AjouterSymbole(Type type); //necessaire pour creer les variables temporelles
        Variable* GetSymbole(string nom) ;

        void AddFunction(string nom, ReturnType retType, vector<Type> params);
        Function* GetFunction(string nom);

        inline int GetOffset()const{
            return offset;
        }
        inline void SetOffset(int newOffset){
            this->offset = newOffset;
        }
        inline int getNbVar(){
            return variables.size();
        }
        inline bool functionExist(string label){
            
            bool ret = false;

            if(functions.find(label) != functions.end())
            {
                ret = true;
            }
            
            return ret;
        }
        map<string, Variable*> GetVariablesCopie() const ;
        void ShowSymboles();


 
    private: 
        map<string, Variable*> variables;

        map<string, Function*> functions;


        int offset;
};

