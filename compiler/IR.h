#ifndef IR_H
#define IR_H

#include <vector>
#include <stack>
#include <string>
#include <iostream>
#include <initializer_list>

#include "TableSymbole.h"

class BasicBlock;
class CFG;
class DefFonction;


typedef enum {
	x86,
	ARM
} OutputType;



//! The class for one 3-address instruction
class IRInstr {

public:
	/** The instructions themselves -- feel free to subclass instead */
	typedef enum {
		ldconst,
		copy,
		add,
		sub,
		mul,
		div,
		mod,
		rmem,
		wmem,
		call, 
		cmp_eq,
		cmp_neq,
		cmp_lt,
		cmp_le,
		ret,
		init_rbp,
		make_not,
		make_neg,	
		op_or,
		op_xor,
		op_and,
	} Operation;


	/**  constructor */
	IRInstr(BasicBlock* bb_, Operation op, Type t, vector<string> params);
	/** destructor **/
	virtual ~IRInstr(); 
	/** Actual code generation */
	virtual void gen_asm(ostream &o, OutputType t) {} /**< assembly code generation for this IR instruction */
	
 protected:
	BasicBlock* bb; /**< The BB this instruction belongs to, which provides a pointer to the CFG this instruction belong to */
	Operation op;
	Type t;
	vector<string> params; /**< For 3-op instrs: d, x, y; for ldconst: d, c;  For call: label, d, params;  for wmem and rmem: choose yourself */
	// if you subclass IRInstr, each IRInstr subclass has its parameters and the previous (very important) comment becomes useless: it would be a better design. 
};


//Classe pour div
class IRInstrDiv: public IRInstr
{
public:
	IRInstrDiv(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = left->offset
	//params[1] = rightVal->offset
	//params[2] = totalVal->offset
};

//Classe pour mod
class IRInstrMod: public IRInstr
{
public:
	IRInstrMod(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = left->offset
	//params[1] = rightVal->offset
	//params[2] = totalVal->offset
};


//Classe pour make_not
class IRInstrNot: public IRInstr
{
public:
	IRInstrNot(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = rightVal->offset
	//params[1] = totalVal->offset
};


//Classe pour make_neg
class IRInstrNeg: public IRInstr
{
public:
	IRInstrNeg(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = rightVal->offset
	//params[1] = totalVal->offset
};


//Classe pour Ldconst
class IRInstrLdconst: public IRInstr
{
public:
	IRInstrLdconst(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = const value 
};


//Classe pour copy
class IRInstrCopy: public IRInstr
{
public:
	IRInstrCopy(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest 
	//params[1] = var
};


//Classe pour add
class IRInstrAdd: public IRInstr
{
public:
	IRInstrAdd(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = var1
	//params[2] = var2
};


//Classe pour sub
class IRInstrSub: public IRInstr
{
public:
	IRInstrSub(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = var1
	//params[2] = var2
};


//Classe pour Multiplication
class IRInstrMul: public IRInstr
{
public:
	IRInstrMul(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = var1
	//params[2] = var2
};


//Classe pour rmem
class IRInstrRmem: public IRInstr
{
public:
	IRInstrRmem(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = addr offset 
};


//Classe pour wmem
class IRInstrWmem: public IRInstr
{
public:
	IRInstrWmem(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = addr offset 
};


//Classe pour call
class IRInstrCall: public IRInstr
{
public:
	IRInstrCall(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = label
	//params[n] = operand n 
};


//Classe pour CmpEq
class IRInstrCmpEq: public IRInstr
{
public:
	IRInstrCmpEq(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = var1 offset
	//params[2] = var2 offset
};


//Classe pour CmpNeq
class IRInstrCmpNeq: public IRInstr
{
public:
	IRInstrCmpNeq(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = var1 offset
	//params[2] = var2 offset
};


//Classe pour Cmplt
class IRInstrCmplt: public IRInstr
{
public:
	IRInstrCmplt(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = var1 offset
	//params[2] = var2 offset
};


//Classe pour Cmple
class IRInstrCmple: public IRInstr
{
public:
	IRInstrCmple(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = var1 offset
	//params[2] = var2 offset
};


//Classe pour Ou
class IRInstrOr: public IRInstr
{
public:
	IRInstrOr(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = var1 offset
	//params[2] = var2 offset
};


//Classe pour And
class IRInstrAnd: public IRInstr
{
public:
	IRInstrAnd(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = var1 offset
	//params[2] = var2 offset
};


//Classe pour Oux
class IRInstrXor: public IRInstr
{
public:
	IRInstrXor(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[0] = dest offset
	//params[1] = var1 offset
	//params[2] = var2 offset
};


//Classe pour Ret
class IRInstrRet: public IRInstr
{
public:
	IRInstrRet(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);


	//params[0] la valeur/variable a retourner (son offset)

};


//Classe pour init_rbp
class IRInstrInit_rbp: public IRInstr
{
public:
	IRInstrInit_rbp(BasicBlock* bb, Type t, vector<string> params);

	void gen_asm(ostream &o, OutputType t = OutputType::x86);

	//params[n] = params
};





/**  The class for a basic block */ 

/* A few important comments.
	 IRInstr has no jump instructions.
	 cmp_* instructions behaves as an arithmetic two-operand instruction (add or mult),
	  returning a boolean value (as an int)

	 Assembly jumps are generated as follows:
	 BasicBlock::gen_asm() first calls IRInstr::gen_asm() on all its instructions, and then 
		    if  exit_true  is a  nullptr, 
            the epilogue is generated
        else if exit_false is a nullptr, 
          an unconditional jmp to the exit_true branch is generated
				else (we have two successors, hence a branch)
          an instruction comparing the value of test_var_name to true is generated,
					followed by a conditional branch to the exit_false branch,
					followed by an unconditional branch to the exit_true branch
	 The attribute test_var_name itself is defined when converting 
  the if, while, etc of the AST  to IR.

Possible optimization:
     a cmp_* comparison instructions, if it is the last instruction of its block, 
       generates an actual assembly comparison 
       followed by a conditional jump to the exit_false branch
*/

class BasicBlock {
 public:
	BasicBlock(CFG* cfg, string entry_label);
	BasicBlock(CFG* cfg, string entry_label, TableSymbole tableCopie);
	void gen_asm(ostream &o, OutputType t = OutputType::x86);  /**< assembly code generation for this basic block (very simple) */

	void add_IRInstr(IRInstr::Operation op, Type t, vector<string> params);

	// No encapsulation whatsoever here. Feel free to do better.
	BasicBlock* exit_true;  /**< pointer to the next basic block, true branch. If nullptr, return from procedure */ 
	BasicBlock* exit_false; /**< pointer to the next basic block, false branch. If null_ptr, the basic block ends with an unconditional jump */
	string label; /**< label of the BB, also will be the label in the generated code */
	CFG* cfg; /** < the CFG where this block belongs */
	vector<IRInstr*> instrs; /** < the instructions themselves. */
    string test_var_name;  /** < when generating IR code for an if(expr) or while(expr) etc,
													 store here the name of the variable that holds the value of expr */
	TableSymbole* tableSymbole;
 protected:

 
};




/** The class for the control flow graph, also includes the symbol table */

/* A few important comments:
	 The entry block is the one with the same label as the AST function name.
	   (it could be the first of bbs, or it could be defined by an attribute value)
	 The exit block is the one with both exit pointers equal to nullptr.
     (again it could be identified in a more explicit way)

 */
class CFG {
 public:
	CFG();

	void add_bb(BasicBlock* bb); 

	// code generation: could be encapsulated in a processor class in a retargetable compiler
	void gen_asm(ostream &o, OutputType t);
	string IR_reg_to_asm(string varName, OutputType t); /**< helper method: inputs a IR reg or input variable, returns e.g. "-24(%rbp)" for the proper value of 24 */
	void gen_asm_prologue(ostream& o, OutputType t);
	void gen_asm_epilogue(ostream& o, OutputType t);

	// symbol table methods
	void add_to_symbol_table(string name, Type t);
	string create_new_tempvar(Type t);
	string create_new_tempvar(Type t, int value);
	Variable* get_var(string name);

	// basic block management
	string new_BB_name();
	BasicBlock* current_bb;

	bool containsBBWithLabel(string labelName);

	inline int getNbbbs(){
		return bbs.size();
	}
	inline int getNbVars(){
		return current_bb->tableSymbole->getNbVar();
	}
 protected:
	static int nextBBnumber; /**< just for naming */

	vector<BasicBlock*> bbs; /**< all the basic blocks of this CFG*/
};

#endif
