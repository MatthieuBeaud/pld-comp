
// Generated from ifcc.g4 by ANTLR 4.7.2


#include "ifccVisitor.h"

#include "ifccParser.h"


using namespace antlrcpp;
using namespace antlr4;

ifccParser::ifccParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

ifccParser::~ifccParser() {
  delete _interpreter;
}

std::string ifccParser::getGrammarFileName() const {
  return "ifcc.g4";
}

const std::vector<std::string>& ifccParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& ifccParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- AxiomContext ------------------------------------------------------------------

ifccParser::AxiomContext::AxiomContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

ifccParser::ProgContext* ifccParser::AxiomContext::prog() {
  return getRuleContext<ifccParser::ProgContext>(0);
}


size_t ifccParser::AxiomContext::getRuleIndex() const {
  return ifccParser::RuleAxiom;
}

antlrcpp::Any ifccParser::AxiomContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitAxiom(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::AxiomContext* ifccParser::axiom() {
  AxiomContext *_localctx = _tracker.createInstance<AxiomContext>(_ctx, getState());
  enterRule(_localctx, 0, ifccParser::RuleAxiom);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(42);
    prog();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProgContext ------------------------------------------------------------------

ifccParser::ProgContext::ProgContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

ifccParser::MainContext* ifccParser::ProgContext::main() {
  return getRuleContext<ifccParser::MainContext>(0);
}

std::vector<ifccParser::DirectiveContext *> ifccParser::ProgContext::directive() {
  return getRuleContexts<ifccParser::DirectiveContext>();
}

ifccParser::DirectiveContext* ifccParser::ProgContext::directive(size_t i) {
  return getRuleContext<ifccParser::DirectiveContext>(i);
}

std::vector<ifccParser::FunctionContext *> ifccParser::ProgContext::function() {
  return getRuleContexts<ifccParser::FunctionContext>();
}

ifccParser::FunctionContext* ifccParser::ProgContext::function(size_t i) {
  return getRuleContext<ifccParser::FunctionContext>(i);
}


size_t ifccParser::ProgContext::getRuleIndex() const {
  return ifccParser::RuleProg;
}

antlrcpp::Any ifccParser::ProgContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitProg(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::ProgContext* ifccParser::prog() {
  ProgContext *_localctx = _tracker.createInstance<ProgContext>(_ctx, getState());
  enterRule(_localctx, 2, ifccParser::RuleProg);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(47);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == ifccParser::DIRECTIVE) {
      setState(44);
      directive();
      setState(49);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(53);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 1, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(50);
        function(); 
      }
      setState(55);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 1, _ctx);
    }
    setState(56);
    main();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MainContext ------------------------------------------------------------------

ifccParser::MainContext::MainContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* ifccParser::MainContext::TYPE() {
  return getToken(ifccParser::TYPE, 0);
}

ifccParser::CodeContext* ifccParser::MainContext::code() {
  return getRuleContext<ifccParser::CodeContext>(0);
}


size_t ifccParser::MainContext::getRuleIndex() const {
  return ifccParser::RuleMain;
}

antlrcpp::Any ifccParser::MainContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitMain(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::MainContext* ifccParser::main() {
  MainContext *_localctx = _tracker.createInstance<MainContext>(_ctx, getState());
  enterRule(_localctx, 4, ifccParser::RuleMain);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(58);
    match(ifccParser::TYPE);
    setState(59);
    match(ifccParser::T__0);
    setState(60);
    match(ifccParser::T__1);
    setState(61);
    match(ifccParser::T__2);
    setState(62);
    match(ifccParser::T__3);
    setState(63);
    code();
    setState(64);
    match(ifccParser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionContext ------------------------------------------------------------------

ifccParser::FunctionContext::FunctionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t ifccParser::FunctionContext::getRuleIndex() const {
  return ifccParser::RuleFunction;
}

void ifccParser::FunctionContext::copyFrom(FunctionContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- DeclarationSansParamsContext ------------------------------------------------------------------

ifccParser::FunctionTypeContext* ifccParser::DeclarationSansParamsContext::functionType() {
  return getRuleContext<ifccParser::FunctionTypeContext>(0);
}

tree::TerminalNode* ifccParser::DeclarationSansParamsContext::VARIABLE() {
  return getToken(ifccParser::VARIABLE, 0);
}

ifccParser::CodeContext* ifccParser::DeclarationSansParamsContext::code() {
  return getRuleContext<ifccParser::CodeContext>(0);
}

ifccParser::DeclarationSansParamsContext::DeclarationSansParamsContext(FunctionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::DeclarationSansParamsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitDeclarationSansParams(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DeclarationAvecParamsContext ------------------------------------------------------------------

ifccParser::FunctionTypeContext* ifccParser::DeclarationAvecParamsContext::functionType() {
  return getRuleContext<ifccParser::FunctionTypeContext>(0);
}

tree::TerminalNode* ifccParser::DeclarationAvecParamsContext::VARIABLE() {
  return getToken(ifccParser::VARIABLE, 0);
}

ifccParser::ParamsContext* ifccParser::DeclarationAvecParamsContext::params() {
  return getRuleContext<ifccParser::ParamsContext>(0);
}

ifccParser::CodeContext* ifccParser::DeclarationAvecParamsContext::code() {
  return getRuleContext<ifccParser::CodeContext>(0);
}

ifccParser::DeclarationAvecParamsContext::DeclarationAvecParamsContext(FunctionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::DeclarationAvecParamsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitDeclarationAvecParams(this);
  else
    return visitor->visitChildren(this);
}
ifccParser::FunctionContext* ifccParser::function() {
  FunctionContext *_localctx = _tracker.createInstance<FunctionContext>(_ctx, getState());
  enterRule(_localctx, 6, ifccParser::RuleFunction);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(83);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 2, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<FunctionContext *>(_tracker.createInstance<ifccParser::DeclarationAvecParamsContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(66);
      functionType();
      setState(67);
      match(ifccParser::VARIABLE);
      setState(68);
      match(ifccParser::T__1);
      setState(69);
      params(0);
      setState(70);
      match(ifccParser::T__2);
      setState(71);
      match(ifccParser::T__3);
      setState(72);
      code();
      setState(73);
      match(ifccParser::T__4);
      break;
    }

    case 2: {
      _localctx = dynamic_cast<FunctionContext *>(_tracker.createInstance<ifccParser::DeclarationSansParamsContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(75);
      functionType();
      setState(76);
      match(ifccParser::VARIABLE);
      setState(77);
      match(ifccParser::T__1);
      setState(78);
      match(ifccParser::T__2);
      setState(79);
      match(ifccParser::T__3);
      setState(80);
      code();
      setState(81);
      match(ifccParser::T__4);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctioncallContext ------------------------------------------------------------------

ifccParser::FunctioncallContext::FunctioncallContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t ifccParser::FunctioncallContext::getRuleIndex() const {
  return ifccParser::RuleFunctioncall;
}

void ifccParser::FunctioncallContext::copyFrom(FunctioncallContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- AppelAvecParamsContext ------------------------------------------------------------------

tree::TerminalNode* ifccParser::AppelAvecParamsContext::VARIABLE() {
  return getToken(ifccParser::VARIABLE, 0);
}

ifccParser::ParamscallContext* ifccParser::AppelAvecParamsContext::paramscall() {
  return getRuleContext<ifccParser::ParamscallContext>(0);
}

ifccParser::AppelAvecParamsContext::AppelAvecParamsContext(FunctioncallContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::AppelAvecParamsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitAppelAvecParams(this);
  else
    return visitor->visitChildren(this);
}
//----------------- AppelSansParamsContext ------------------------------------------------------------------

tree::TerminalNode* ifccParser::AppelSansParamsContext::VARIABLE() {
  return getToken(ifccParser::VARIABLE, 0);
}

ifccParser::AppelSansParamsContext::AppelSansParamsContext(FunctioncallContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::AppelSansParamsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitAppelSansParams(this);
  else
    return visitor->visitChildren(this);
}
ifccParser::FunctioncallContext* ifccParser::functioncall() {
  FunctioncallContext *_localctx = _tracker.createInstance<FunctioncallContext>(_ctx, getState());
  enterRule(_localctx, 8, ifccParser::RuleFunctioncall);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(93);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<FunctioncallContext *>(_tracker.createInstance<ifccParser::AppelAvecParamsContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(85);
      match(ifccParser::VARIABLE);
      setState(86);
      match(ifccParser::T__1);
      setState(87);
      paramscall(0);
      setState(88);
      match(ifccParser::T__2);
      break;
    }

    case 2: {
      _localctx = dynamic_cast<FunctioncallContext *>(_tracker.createInstance<ifccParser::AppelSansParamsContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(90);
      match(ifccParser::VARIABLE);
      setState(91);
      match(ifccParser::T__1);
      setState(92);
      match(ifccParser::T__2);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CodeContext ------------------------------------------------------------------

ifccParser::CodeContext::CodeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

ifccParser::DeclarationContext* ifccParser::CodeContext::declaration() {
  return getRuleContext<ifccParser::DeclarationContext>(0);
}

std::vector<ifccParser::CodeContext *> ifccParser::CodeContext::code() {
  return getRuleContexts<ifccParser::CodeContext>();
}

ifccParser::CodeContext* ifccParser::CodeContext::code(size_t i) {
  return getRuleContext<ifccParser::CodeContext>(i);
}

ifccParser::DefinitionContext* ifccParser::CodeContext::definition() {
  return getRuleContext<ifccParser::DefinitionContext>(0);
}

ifccParser::AffectationContext* ifccParser::CodeContext::affectation() {
  return getRuleContext<ifccParser::AffectationContext>(0);
}

ifccParser::FunctioncallContext* ifccParser::CodeContext::functioncall() {
  return getRuleContext<ifccParser::FunctioncallContext>(0);
}

ifccParser::ConditionContext* ifccParser::CodeContext::condition() {
  return getRuleContext<ifccParser::ConditionContext>(0);
}

ifccParser::BoucleWhileContext* ifccParser::CodeContext::boucleWhile() {
  return getRuleContext<ifccParser::BoucleWhileContext>(0);
}

std::vector<ifccParser::ReturnStatementContext *> ifccParser::CodeContext::returnStatement() {
  return getRuleContexts<ifccParser::ReturnStatementContext>();
}

ifccParser::ReturnStatementContext* ifccParser::CodeContext::returnStatement(size_t i) {
  return getRuleContext<ifccParser::ReturnStatementContext>(i);
}


size_t ifccParser::CodeContext::getRuleIndex() const {
  return ifccParser::RuleCode;
}

antlrcpp::Any ifccParser::CodeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitCode(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::CodeContext* ifccParser::code() {
  CodeContext *_localctx = _tracker.createInstance<CodeContext>(_ctx, getState());
  enterRule(_localctx, 10, ifccParser::RuleCode);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    setState(122);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 5, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(95);
      declaration();
      setState(96);
      code();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(98);
      definition();
      setState(99);
      code();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(101);
      affectation();
      setState(102);
      code();
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(104);
      functioncall();
      setState(105);
      match(ifccParser::T__5);
      setState(106);
      code();
      break;
    }

    case 5: {
      enterOuterAlt(_localctx, 5);
      setState(108);
      condition();
      setState(109);
      code();
      break;
    }

    case 6: {
      enterOuterAlt(_localctx, 6);
      setState(111);
      boucleWhile();
      setState(112);
      code();
      break;
    }

    case 7: {
      enterOuterAlt(_localctx, 7);
      setState(119);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx);
      while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
        if (alt == 1) {
          setState(114);
          returnStatement();
          setState(115);
          code(); 
        }
        setState(121);
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx);
      }
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ReturnStatementContext ------------------------------------------------------------------

ifccParser::ReturnStatementContext::ReturnStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t ifccParser::ReturnStatementContext::getRuleIndex() const {
  return ifccParser::RuleReturnStatement;
}

void ifccParser::ReturnStatementContext::copyFrom(ReturnStatementContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ReturnVoidContext ------------------------------------------------------------------

tree::TerminalNode* ifccParser::ReturnVoidContext::RETURN() {
  return getToken(ifccParser::RETURN, 0);
}

ifccParser::ReturnVoidContext::ReturnVoidContext(ReturnStatementContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::ReturnVoidContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitReturnVoid(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ReturnExprContext ------------------------------------------------------------------

tree::TerminalNode* ifccParser::ReturnExprContext::RETURN() {
  return getToken(ifccParser::RETURN, 0);
}

ifccParser::ExpressionContext* ifccParser::ReturnExprContext::expression() {
  return getRuleContext<ifccParser::ExpressionContext>(0);
}

ifccParser::ReturnExprContext::ReturnExprContext(ReturnStatementContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::ReturnExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitReturnExpr(this);
  else
    return visitor->visitChildren(this);
}
ifccParser::ReturnStatementContext* ifccParser::returnStatement() {
  ReturnStatementContext *_localctx = _tracker.createInstance<ReturnStatementContext>(_ctx, getState());
  enterRule(_localctx, 12, ifccParser::RuleReturnStatement);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(130);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 6, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<ReturnStatementContext *>(_tracker.createInstance<ifccParser::ReturnVoidContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(124);
      match(ifccParser::RETURN);
      setState(125);
      match(ifccParser::T__5);
      break;
    }

    case 2: {
      _localctx = dynamic_cast<ReturnStatementContext *>(_tracker.createInstance<ifccParser::ReturnExprContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(126);
      match(ifccParser::RETURN);
      setState(127);
      expression(0);
      setState(128);
      match(ifccParser::T__5);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DirectiveContext ------------------------------------------------------------------

ifccParser::DirectiveContext::DirectiveContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* ifccParser::DirectiveContext::DIRECTIVE() {
  return getToken(ifccParser::DIRECTIVE, 0);
}

ifccParser::DirectiveContext* ifccParser::DirectiveContext::directive() {
  return getRuleContext<ifccParser::DirectiveContext>(0);
}


size_t ifccParser::DirectiveContext::getRuleIndex() const {
  return ifccParser::RuleDirective;
}

antlrcpp::Any ifccParser::DirectiveContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitDirective(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::DirectiveContext* ifccParser::directive() {
  DirectiveContext *_localctx = _tracker.createInstance<DirectiveContext>(_ctx, getState());
  enterRule(_localctx, 14, ifccParser::RuleDirective);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(135);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 7, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(132);
      match(ifccParser::DIRECTIVE);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(133);
      match(ifccParser::DIRECTIVE);
      setState(134);
      directive();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DeclarationContext ------------------------------------------------------------------

ifccParser::DeclarationContext::DeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* ifccParser::DeclarationContext::TYPE() {
  return getToken(ifccParser::TYPE, 0);
}

ifccParser::MultivarContext* ifccParser::DeclarationContext::multivar() {
  return getRuleContext<ifccParser::MultivarContext>(0);
}


size_t ifccParser::DeclarationContext::getRuleIndex() const {
  return ifccParser::RuleDeclaration;
}

antlrcpp::Any ifccParser::DeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitDeclaration(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::DeclarationContext* ifccParser::declaration() {
  DeclarationContext *_localctx = _tracker.createInstance<DeclarationContext>(_ctx, getState());
  enterRule(_localctx, 16, ifccParser::RuleDeclaration);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(137);
    match(ifccParser::TYPE);
    setState(138);
    multivar(0);
    setState(139);
    match(ifccParser::T__5);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DefinitionContext ------------------------------------------------------------------

ifccParser::DefinitionContext::DefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* ifccParser::DefinitionContext::TYPE() {
  return getToken(ifccParser::TYPE, 0);
}

tree::TerminalNode* ifccParser::DefinitionContext::VARIABLE() {
  return getToken(ifccParser::VARIABLE, 0);
}

ifccParser::ExpressionContext* ifccParser::DefinitionContext::expression() {
  return getRuleContext<ifccParser::ExpressionContext>(0);
}


size_t ifccParser::DefinitionContext::getRuleIndex() const {
  return ifccParser::RuleDefinition;
}

antlrcpp::Any ifccParser::DefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitDefinition(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::DefinitionContext* ifccParser::definition() {
  DefinitionContext *_localctx = _tracker.createInstance<DefinitionContext>(_ctx, getState());
  enterRule(_localctx, 18, ifccParser::RuleDefinition);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(141);
    match(ifccParser::TYPE);
    setState(142);
    match(ifccParser::VARIABLE);
    setState(143);
    match(ifccParser::T__6);
    setState(144);
    expression(0);
    setState(145);
    match(ifccParser::T__5);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AffectationContext ------------------------------------------------------------------

ifccParser::AffectationContext::AffectationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* ifccParser::AffectationContext::VARIABLE() {
  return getToken(ifccParser::VARIABLE, 0);
}

ifccParser::ExpressionContext* ifccParser::AffectationContext::expression() {
  return getRuleContext<ifccParser::ExpressionContext>(0);
}


size_t ifccParser::AffectationContext::getRuleIndex() const {
  return ifccParser::RuleAffectation;
}

antlrcpp::Any ifccParser::AffectationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitAffectation(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::AffectationContext* ifccParser::affectation() {
  AffectationContext *_localctx = _tracker.createInstance<AffectationContext>(_ctx, getState());
  enterRule(_localctx, 20, ifccParser::RuleAffectation);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(147);
    match(ifccParser::VARIABLE);
    setState(148);
    match(ifccParser::T__6);
    setState(149);
    expression(0);
    setState(150);
    match(ifccParser::T__5);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionContext ------------------------------------------------------------------

ifccParser::ExpressionContext::ExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t ifccParser::ExpressionContext::getRuleIndex() const {
  return ifccParser::RuleExpression;
}

void ifccParser::ExpressionContext::copyFrom(ExpressionContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ParContext ------------------------------------------------------------------

ifccParser::ExpressionContext* ifccParser::ParContext::expression() {
  return getRuleContext<ifccParser::ExpressionContext>(0);
}

ifccParser::ParContext::ParContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::ParContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitPar(this);
  else
    return visitor->visitChildren(this);
}
//----------------- AddContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::AddContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::AddContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::AddContext::AddContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::AddContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitAdd(this);
  else
    return visitor->visitChildren(this);
}
//----------------- SubContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::SubContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::SubContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::SubContext::SubContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::SubContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitSub(this);
  else
    return visitor->visitChildren(this);
}
//----------------- MultContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::MultContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::MultContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::MultContext::MultContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::MultContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitMult(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ModContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::ModContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::ModContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::ModContext::ModContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::ModContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitMod(this);
  else
    return visitor->visitChildren(this);
}
//----------------- OrContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::OrContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::OrContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::OrContext::OrContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::OrContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitOr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ValeurContext ------------------------------------------------------------------

ifccParser::ValContext* ifccParser::ValeurContext::val() {
  return getRuleContext<ifccParser::ValContext>(0);
}

ifccParser::ValeurContext::ValeurContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::ValeurContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitValeur(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LessContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::LessContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::LessContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::LessContext::LessContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::LessContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitLess(this);
  else
    return visitor->visitChildren(this);
}
//----------------- OppContext ------------------------------------------------------------------

ifccParser::ExpressionContext* ifccParser::OppContext::expression() {
  return getRuleContext<ifccParser::ExpressionContext>(0);
}

ifccParser::OppContext::OppContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::OppContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitOpp(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CallContext ------------------------------------------------------------------

ifccParser::FunctioncallContext* ifccParser::CallContext::functioncall() {
  return getRuleContext<ifccParser::FunctioncallContext>(0);
}

ifccParser::CallContext::CallContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::CallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitCall(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DivContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::DivContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::DivContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::DivContext::DivContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::DivContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitDiv(this);
  else
    return visitor->visitChildren(this);
}
//----------------- EqualContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::EqualContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::EqualContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::EqualContext::EqualContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::EqualContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitEqual(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NotContext ------------------------------------------------------------------

ifccParser::ExpressionContext* ifccParser::NotContext::expression() {
  return getRuleContext<ifccParser::ExpressionContext>(0);
}

ifccParser::NotContext::NotContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::NotContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitNot(this);
  else
    return visitor->visitChildren(this);
}
//----------------- AndContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::AndContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::AndContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::AndContext::AndContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::AndContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitAnd(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LessequalContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::LessequalContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::LessequalContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::LessequalContext::LessequalContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::LessequalContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitLessequal(this);
  else
    return visitor->visitChildren(this);
}
//----------------- GreaterequalContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::GreaterequalContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::GreaterequalContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::GreaterequalContext::GreaterequalContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::GreaterequalContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitGreaterequal(this);
  else
    return visitor->visitChildren(this);
}
//----------------- XorContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::XorContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::XorContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::XorContext::XorContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::XorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitXor(this);
  else
    return visitor->visitChildren(this);
}
//----------------- GreaterContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::GreaterContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::GreaterContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::GreaterContext::GreaterContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::GreaterContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitGreater(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NequalContext ------------------------------------------------------------------

std::vector<ifccParser::ExpressionContext *> ifccParser::NequalContext::expression() {
  return getRuleContexts<ifccParser::ExpressionContext>();
}

ifccParser::ExpressionContext* ifccParser::NequalContext::expression(size_t i) {
  return getRuleContext<ifccParser::ExpressionContext>(i);
}

ifccParser::NequalContext::NequalContext(ExpressionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any ifccParser::NequalContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitNequal(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::ExpressionContext* ifccParser::expression() {
   return expression(0);
}

ifccParser::ExpressionContext* ifccParser::expression(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  ifccParser::ExpressionContext *_localctx = _tracker.createInstance<ExpressionContext>(_ctx, parentState);
  ifccParser::ExpressionContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 22;
  enterRecursionRule(_localctx, 22, ifccParser::RuleExpression, precedence);

    

  auto onExit = finally([=] {
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(163);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 8, _ctx)) {
    case 1: {
      _localctx = _tracker.createInstance<ValeurContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;

      setState(153);
      val();
      break;
    }

    case 2: {
      _localctx = _tracker.createInstance<NotContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(154);
      match(ifccParser::T__7);
      setState(155);
      expression(20);
      break;
    }

    case 3: {
      _localctx = _tracker.createInstance<OppContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(156);
      match(ifccParser::T__8);
      setState(157);
      expression(19);
      break;
    }

    case 4: {
      _localctx = _tracker.createInstance<CallContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(158);
      functioncall();
      break;
    }

    case 5: {
      _localctx = _tracker.createInstance<ParContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(159);
      match(ifccParser::T__1);
      setState(160);
      expression(0);
      setState(161);
      match(ifccParser::T__2);
      break;
    }

    }
    _ctx->stop = _input->LT(-1);
    setState(215);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 10, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(213);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 9, _ctx)) {
        case 1: {
          auto newContext = _tracker.createInstance<MultContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(165);

          if (!(precpred(_ctx, 18))) throw FailedPredicateException(this, "precpred(_ctx, 18)");
          setState(166);
          match(ifccParser::T__9);
          setState(167);
          expression(19);
          break;
        }

        case 2: {
          auto newContext = _tracker.createInstance<DivContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(168);

          if (!(precpred(_ctx, 17))) throw FailedPredicateException(this, "precpred(_ctx, 17)");
          setState(169);
          match(ifccParser::T__10);
          setState(170);
          expression(18);
          break;
        }

        case 3: {
          auto newContext = _tracker.createInstance<ModContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(171);

          if (!(precpred(_ctx, 16))) throw FailedPredicateException(this, "precpred(_ctx, 16)");
          setState(172);
          match(ifccParser::T__11);
          setState(173);
          expression(17);
          break;
        }

        case 4: {
          auto newContext = _tracker.createInstance<AddContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(174);

          if (!(precpred(_ctx, 15))) throw FailedPredicateException(this, "precpred(_ctx, 15)");
          setState(175);
          match(ifccParser::T__12);
          setState(176);
          expression(16);
          break;
        }

        case 5: {
          auto newContext = _tracker.createInstance<SubContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(177);

          if (!(precpred(_ctx, 14))) throw FailedPredicateException(this, "precpred(_ctx, 14)");
          setState(178);
          match(ifccParser::T__8);
          setState(179);
          expression(15);
          break;
        }

        case 6: {
          auto newContext = _tracker.createInstance<AndContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(180);

          if (!(precpred(_ctx, 13))) throw FailedPredicateException(this, "precpred(_ctx, 13)");
          setState(181);
          match(ifccParser::T__13);
          setState(182);
          expression(14);
          break;
        }

        case 7: {
          auto newContext = _tracker.createInstance<AndContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(183);

          if (!(precpred(_ctx, 12))) throw FailedPredicateException(this, "precpred(_ctx, 12)");
          setState(184);
          match(ifccParser::T__14);
          setState(185);
          expression(13);
          break;
        }

        case 8: {
          auto newContext = _tracker.createInstance<OrContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(186);

          if (!(precpred(_ctx, 11))) throw FailedPredicateException(this, "precpred(_ctx, 11)");
          setState(187);
          match(ifccParser::T__15);
          setState(188);
          expression(12);
          break;
        }

        case 9: {
          auto newContext = _tracker.createInstance<OrContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(189);

          if (!(precpred(_ctx, 10))) throw FailedPredicateException(this, "precpred(_ctx, 10)");
          setState(190);
          match(ifccParser::T__16);
          setState(191);
          expression(11);
          break;
        }

        case 10: {
          auto newContext = _tracker.createInstance<XorContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(192);

          if (!(precpred(_ctx, 9))) throw FailedPredicateException(this, "precpred(_ctx, 9)");
          setState(193);
          match(ifccParser::T__17);
          setState(194);
          expression(10);
          break;
        }

        case 11: {
          auto newContext = _tracker.createInstance<EqualContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(195);

          if (!(precpred(_ctx, 8))) throw FailedPredicateException(this, "precpred(_ctx, 8)");
          setState(196);
          match(ifccParser::T__18);
          setState(197);
          expression(9);
          break;
        }

        case 12: {
          auto newContext = _tracker.createInstance<NequalContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(198);

          if (!(precpred(_ctx, 7))) throw FailedPredicateException(this, "precpred(_ctx, 7)");
          setState(199);
          match(ifccParser::T__19);
          setState(200);
          expression(8);
          break;
        }

        case 13: {
          auto newContext = _tracker.createInstance<GreaterContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(201);

          if (!(precpred(_ctx, 6))) throw FailedPredicateException(this, "precpred(_ctx, 6)");
          setState(202);
          match(ifccParser::T__20);
          setState(203);
          expression(7);
          break;
        }

        case 14: {
          auto newContext = _tracker.createInstance<LessContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(204);

          if (!(precpred(_ctx, 5))) throw FailedPredicateException(this, "precpred(_ctx, 5)");
          setState(205);
          match(ifccParser::T__21);
          setState(206);
          expression(6);
          break;
        }

        case 15: {
          auto newContext = _tracker.createInstance<GreaterequalContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(207);

          if (!(precpred(_ctx, 4))) throw FailedPredicateException(this, "precpred(_ctx, 4)");
          setState(208);
          match(ifccParser::T__22);
          setState(209);
          expression(5);
          break;
        }

        case 16: {
          auto newContext = _tracker.createInstance<LessequalContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(210);

          if (!(precpred(_ctx, 3))) throw FailedPredicateException(this, "precpred(_ctx, 3)");
          setState(211);
          match(ifccParser::T__23);
          setState(212);
          expression(4);
          break;
        }

        } 
      }
      setState(217);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 10, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- BoucleWhileContext ------------------------------------------------------------------

ifccParser::BoucleWhileContext::BoucleWhileContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

ifccParser::ExpressionContext* ifccParser::BoucleWhileContext::expression() {
  return getRuleContext<ifccParser::ExpressionContext>(0);
}

ifccParser::CodeContext* ifccParser::BoucleWhileContext::code() {
  return getRuleContext<ifccParser::CodeContext>(0);
}


size_t ifccParser::BoucleWhileContext::getRuleIndex() const {
  return ifccParser::RuleBoucleWhile;
}

antlrcpp::Any ifccParser::BoucleWhileContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitBoucleWhile(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::BoucleWhileContext* ifccParser::boucleWhile() {
  BoucleWhileContext *_localctx = _tracker.createInstance<BoucleWhileContext>(_ctx, getState());
  enterRule(_localctx, 24, ifccParser::RuleBoucleWhile);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(218);
    match(ifccParser::T__24);
    setState(219);
    match(ifccParser::T__1);
    setState(220);
    expression(0);
    setState(221);
    match(ifccParser::T__2);
    setState(222);
    match(ifccParser::T__3);
    setState(223);
    code();
    setState(224);
    match(ifccParser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConditionContext ------------------------------------------------------------------

ifccParser::ConditionContext::ConditionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

ifccParser::ExpressionContext* ifccParser::ConditionContext::expression() {
  return getRuleContext<ifccParser::ExpressionContext>(0);
}

ifccParser::CodeContext* ifccParser::ConditionContext::code() {
  return getRuleContext<ifccParser::CodeContext>(0);
}

ifccParser::AlternativeContext* ifccParser::ConditionContext::alternative() {
  return getRuleContext<ifccParser::AlternativeContext>(0);
}


size_t ifccParser::ConditionContext::getRuleIndex() const {
  return ifccParser::RuleCondition;
}

antlrcpp::Any ifccParser::ConditionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitCondition(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::ConditionContext* ifccParser::condition() {
  ConditionContext *_localctx = _tracker.createInstance<ConditionContext>(_ctx, getState());
  enterRule(_localctx, 26, ifccParser::RuleCondition);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(243);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 11, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(226);
      match(ifccParser::T__25);
      setState(227);
      match(ifccParser::T__1);
      setState(228);
      expression(0);
      setState(229);
      match(ifccParser::T__2);
      setState(230);
      match(ifccParser::T__3);
      setState(231);
      code();
      setState(232);
      match(ifccParser::T__4);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(234);
      match(ifccParser::T__25);
      setState(235);
      match(ifccParser::T__1);
      setState(236);
      expression(0);
      setState(237);
      match(ifccParser::T__2);
      setState(238);
      match(ifccParser::T__3);
      setState(239);
      code();
      setState(240);
      match(ifccParser::T__4);
      setState(241);
      alternative();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AlternativeContext ------------------------------------------------------------------

ifccParser::AlternativeContext::AlternativeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

ifccParser::CodeContext* ifccParser::AlternativeContext::code() {
  return getRuleContext<ifccParser::CodeContext>(0);
}


size_t ifccParser::AlternativeContext::getRuleIndex() const {
  return ifccParser::RuleAlternative;
}

antlrcpp::Any ifccParser::AlternativeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitAlternative(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::AlternativeContext* ifccParser::alternative() {
  AlternativeContext *_localctx = _tracker.createInstance<AlternativeContext>(_ctx, getState());
  enterRule(_localctx, 28, ifccParser::RuleAlternative);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(245);
    match(ifccParser::T__26);
    setState(246);
    match(ifccParser::T__3);
    setState(247);
    code();
    setState(248);
    match(ifccParser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ValContext ------------------------------------------------------------------

ifccParser::ValContext::ValContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* ifccParser::ValContext::VARIABLE() {
  return getToken(ifccParser::VARIABLE, 0);
}

tree::TerminalNode* ifccParser::ValContext::CONST() {
  return getToken(ifccParser::CONST, 0);
}


size_t ifccParser::ValContext::getRuleIndex() const {
  return ifccParser::RuleVal;
}

antlrcpp::Any ifccParser::ValContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitVal(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::ValContext* ifccParser::val() {
  ValContext *_localctx = _tracker.createInstance<ValContext>(_ctx, getState());
  enterRule(_localctx, 30, ifccParser::RuleVal);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(250);
    _la = _input->LA(1);
    if (!(_la == ifccParser::VARIABLE

    || _la == ifccParser::CONST)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MultivarContext ------------------------------------------------------------------

ifccParser::MultivarContext::MultivarContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* ifccParser::MultivarContext::VARIABLE() {
  return getToken(ifccParser::VARIABLE, 0);
}

ifccParser::MultivarContext* ifccParser::MultivarContext::multivar() {
  return getRuleContext<ifccParser::MultivarContext>(0);
}


size_t ifccParser::MultivarContext::getRuleIndex() const {
  return ifccParser::RuleMultivar;
}

antlrcpp::Any ifccParser::MultivarContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitMultivar(this);
  else
    return visitor->visitChildren(this);
}


ifccParser::MultivarContext* ifccParser::multivar() {
   return multivar(0);
}

ifccParser::MultivarContext* ifccParser::multivar(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  ifccParser::MultivarContext *_localctx = _tracker.createInstance<MultivarContext>(_ctx, parentState);
  ifccParser::MultivarContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 32;
  enterRecursionRule(_localctx, 32, ifccParser::RuleMultivar, precedence);

    

  auto onExit = finally([=] {
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(253);
    match(ifccParser::VARIABLE);
    _ctx->stop = _input->LT(-1);
    setState(260);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        _localctx = _tracker.createInstance<MultivarContext>(parentContext, parentState);
        pushNewRecursionContext(_localctx, startState, RuleMultivar);
        setState(255);

        if (!(precpred(_ctx, 1))) throw FailedPredicateException(this, "precpred(_ctx, 1)");
        setState(256);
        match(ifccParser::T__27);
        setState(257);
        match(ifccParser::VARIABLE); 
      }
      setState(262);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- ParamsContext ------------------------------------------------------------------

ifccParser::ParamsContext::ParamsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

ifccParser::ParamContext* ifccParser::ParamsContext::param() {
  return getRuleContext<ifccParser::ParamContext>(0);
}

ifccParser::ParamsContext* ifccParser::ParamsContext::params() {
  return getRuleContext<ifccParser::ParamsContext>(0);
}


size_t ifccParser::ParamsContext::getRuleIndex() const {
  return ifccParser::RuleParams;
}

antlrcpp::Any ifccParser::ParamsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitParams(this);
  else
    return visitor->visitChildren(this);
}


ifccParser::ParamsContext* ifccParser::params() {
   return params(0);
}

ifccParser::ParamsContext* ifccParser::params(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  ifccParser::ParamsContext *_localctx = _tracker.createInstance<ParamsContext>(_ctx, parentState);
  ifccParser::ParamsContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 34;
  enterRecursionRule(_localctx, 34, ifccParser::RuleParams, precedence);

    

  auto onExit = finally([=] {
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(264);
    param();
    _ctx->stop = _input->LT(-1);
    setState(271);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 13, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        _localctx = _tracker.createInstance<ParamsContext>(parentContext, parentState);
        pushNewRecursionContext(_localctx, startState, RuleParams);
        setState(266);

        if (!(precpred(_ctx, 1))) throw FailedPredicateException(this, "precpred(_ctx, 1)");
        setState(267);
        match(ifccParser::T__27);
        setState(268);
        param(); 
      }
      setState(273);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 13, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- ParamContext ------------------------------------------------------------------

ifccParser::ParamContext::ParamContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* ifccParser::ParamContext::TYPE() {
  return getToken(ifccParser::TYPE, 0);
}

tree::TerminalNode* ifccParser::ParamContext::VARIABLE() {
  return getToken(ifccParser::VARIABLE, 0);
}


size_t ifccParser::ParamContext::getRuleIndex() const {
  return ifccParser::RuleParam;
}

antlrcpp::Any ifccParser::ParamContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitParam(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::ParamContext* ifccParser::param() {
  ParamContext *_localctx = _tracker.createInstance<ParamContext>(_ctx, getState());
  enterRule(_localctx, 36, ifccParser::RuleParam);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(274);
    match(ifccParser::TYPE);
    setState(275);
    match(ifccParser::VARIABLE);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParamscallContext ------------------------------------------------------------------

ifccParser::ParamscallContext::ParamscallContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

ifccParser::ExpressionContext* ifccParser::ParamscallContext::expression() {
  return getRuleContext<ifccParser::ExpressionContext>(0);
}

ifccParser::ParamscallContext* ifccParser::ParamscallContext::paramscall() {
  return getRuleContext<ifccParser::ParamscallContext>(0);
}


size_t ifccParser::ParamscallContext::getRuleIndex() const {
  return ifccParser::RuleParamscall;
}

antlrcpp::Any ifccParser::ParamscallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitParamscall(this);
  else
    return visitor->visitChildren(this);
}


ifccParser::ParamscallContext* ifccParser::paramscall() {
   return paramscall(0);
}

ifccParser::ParamscallContext* ifccParser::paramscall(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  ifccParser::ParamscallContext *_localctx = _tracker.createInstance<ParamscallContext>(_ctx, parentState);
  ifccParser::ParamscallContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 38;
  enterRecursionRule(_localctx, 38, ifccParser::RuleParamscall, precedence);

    

  auto onExit = finally([=] {
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(278);
    expression(0);
    _ctx->stop = _input->LT(-1);
    setState(285);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 14, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        _localctx = _tracker.createInstance<ParamscallContext>(parentContext, parentState);
        pushNewRecursionContext(_localctx, startState, RuleParamscall);
        setState(280);

        if (!(precpred(_ctx, 1))) throw FailedPredicateException(this, "precpred(_ctx, 1)");
        setState(281);
        match(ifccParser::T__27);
        setState(282);
        expression(0); 
      }
      setState(287);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 14, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- FunctionTypeContext ------------------------------------------------------------------

ifccParser::FunctionTypeContext::FunctionTypeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* ifccParser::FunctionTypeContext::TYPE() {
  return getToken(ifccParser::TYPE, 0);
}


size_t ifccParser::FunctionTypeContext::getRuleIndex() const {
  return ifccParser::RuleFunctionType;
}

antlrcpp::Any ifccParser::FunctionTypeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<ifccVisitor*>(visitor))
    return parserVisitor->visitFunctionType(this);
  else
    return visitor->visitChildren(this);
}

ifccParser::FunctionTypeContext* ifccParser::functionType() {
  FunctionTypeContext *_localctx = _tracker.createInstance<FunctionTypeContext>(_ctx, getState());
  enterRule(_localctx, 40, ifccParser::RuleFunctionType);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(288);
    _la = _input->LA(1);
    if (!(_la == ifccParser::T__28

    || _la == ifccParser::TYPE)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

bool ifccParser::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 11: return expressionSempred(dynamic_cast<ExpressionContext *>(context), predicateIndex);
    case 16: return multivarSempred(dynamic_cast<MultivarContext *>(context), predicateIndex);
    case 17: return paramsSempred(dynamic_cast<ParamsContext *>(context), predicateIndex);
    case 19: return paramscallSempred(dynamic_cast<ParamscallContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool ifccParser::expressionSempred(ExpressionContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 18);
    case 1: return precpred(_ctx, 17);
    case 2: return precpred(_ctx, 16);
    case 3: return precpred(_ctx, 15);
    case 4: return precpred(_ctx, 14);
    case 5: return precpred(_ctx, 13);
    case 6: return precpred(_ctx, 12);
    case 7: return precpred(_ctx, 11);
    case 8: return precpred(_ctx, 10);
    case 9: return precpred(_ctx, 9);
    case 10: return precpred(_ctx, 8);
    case 11: return precpred(_ctx, 7);
    case 12: return precpred(_ctx, 6);
    case 13: return precpred(_ctx, 5);
    case 14: return precpred(_ctx, 4);
    case 15: return precpred(_ctx, 3);

  default:
    break;
  }
  return true;
}

bool ifccParser::multivarSempred(MultivarContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 16: return precpred(_ctx, 1);

  default:
    break;
  }
  return true;
}

bool ifccParser::paramsSempred(ParamsContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 17: return precpred(_ctx, 1);

  default:
    break;
  }
  return true;
}

bool ifccParser::paramscallSempred(ParamscallContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 18: return precpred(_ctx, 1);

  default:
    break;
  }
  return true;
}

// Static vars and initialization.
std::vector<dfa::DFA> ifccParser::_decisionToDFA;
atn::PredictionContextCache ifccParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN ifccParser::_atn;
std::vector<uint16_t> ifccParser::_serializedATN;

std::vector<std::string> ifccParser::_ruleNames = {
  "axiom", "prog", "main", "function", "functioncall", "code", "returnStatement", 
  "directive", "declaration", "definition", "affectation", "expression", 
  "boucleWhile", "condition", "alternative", "val", "multivar", "params", 
  "param", "paramscall", "functionType"
};

std::vector<std::string> ifccParser::_literalNames = {
  "", "'main'", "'('", "')'", "'{'", "'}'", "';'", "'='", "'!'", "'-'", 
  "'*'", "'/'", "'%'", "'+'", "'&'", "'&&'", "'||'", "'|'", "'^'", "'=='", 
  "'!='", "'>'", "'<'", "'>='", "'<='", "'while'", "'if'", "'else'", "','", 
  "'void'", "'int'", "'return'"
};

std::vector<std::string> ifccParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "TYPE", "RETURN", "VARIABLE", 
  "CONST", "COMMENT", "DIRECTIVE", "WS"
};

dfa::Vocabulary ifccParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> ifccParser::_tokenNames;

ifccParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x26, 0x125, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x4, 0x15, 
    0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x3, 0x2, 0x3, 0x2, 0x3, 0x3, 0x7, 
    0x3, 0x30, 0xa, 0x3, 0xc, 0x3, 0xe, 0x3, 0x33, 0xb, 0x3, 0x3, 0x3, 0x7, 
    0x3, 0x36, 0xa, 0x3, 0xc, 0x3, 0xe, 0x3, 0x39, 0xb, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 
    0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 
    0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x5, 0x5, 0x56, 0xa, 0x5, 
    0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 
    0x3, 0x6, 0x5, 0x6, 0x60, 0xa, 0x6, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 
    0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 
    0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 
    0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x7, 0x7, 0x78, 0xa, 0x7, 
    0xc, 0x7, 0xe, 0x7, 0x7b, 0xb, 0x7, 0x5, 0x7, 0x7d, 0xa, 0x7, 0x3, 0x8, 
    0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x5, 0x8, 0x85, 0xa, 
    0x8, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x5, 0x9, 0x8a, 0xa, 0x9, 0x3, 0xa, 
    0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 
    0x3, 0xb, 0x3, 0xb, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 
    0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 
    0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x5, 0xd, 0xa6, 0xa, 0xd, 0x3, 
    0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 
    0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 
    0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 
    0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 
    0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 
    0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 
    0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x7, 0xd, 0xd8, 
    0xa, 0xd, 0xc, 0xd, 0xe, 0xd, 0xdb, 0xb, 0xd, 0x3, 0xe, 0x3, 0xe, 0x3, 
    0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xf, 0x3, 
    0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 
    0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 
    0xf, 0x3, 0xf, 0x5, 0xf, 0xf6, 0xa, 0xf, 0x3, 0x10, 0x3, 0x10, 0x3, 
    0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 0x12, 0x3, 0x12, 
    0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x7, 0x12, 0x105, 0xa, 0x12, 
    0xc, 0x12, 0xe, 0x12, 0x108, 0xb, 0x12, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x7, 0x13, 0x110, 0xa, 0x13, 0xc, 0x13, 
    0xe, 0x13, 0x113, 0xb, 0x13, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 0x3, 0x15, 
    0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x7, 0x15, 0x11e, 
    0xa, 0x15, 0xc, 0x15, 0xe, 0x15, 0x121, 0xb, 0x15, 0x3, 0x16, 0x3, 0x16, 
    0x3, 0x16, 0x2, 0x6, 0x18, 0x22, 0x24, 0x28, 0x17, 0x2, 0x4, 0x6, 0x8, 
    0xa, 0xc, 0xe, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 
    0x22, 0x24, 0x26, 0x28, 0x2a, 0x2, 0x4, 0x3, 0x2, 0x22, 0x23, 0x3, 0x2, 
    0x1f, 0x20, 0x2, 0x134, 0x2, 0x2c, 0x3, 0x2, 0x2, 0x2, 0x4, 0x31, 0x3, 
    0x2, 0x2, 0x2, 0x6, 0x3c, 0x3, 0x2, 0x2, 0x2, 0x8, 0x55, 0x3, 0x2, 0x2, 
    0x2, 0xa, 0x5f, 0x3, 0x2, 0x2, 0x2, 0xc, 0x7c, 0x3, 0x2, 0x2, 0x2, 0xe, 
    0x84, 0x3, 0x2, 0x2, 0x2, 0x10, 0x89, 0x3, 0x2, 0x2, 0x2, 0x12, 0x8b, 
    0x3, 0x2, 0x2, 0x2, 0x14, 0x8f, 0x3, 0x2, 0x2, 0x2, 0x16, 0x95, 0x3, 
    0x2, 0x2, 0x2, 0x18, 0xa5, 0x3, 0x2, 0x2, 0x2, 0x1a, 0xdc, 0x3, 0x2, 
    0x2, 0x2, 0x1c, 0xf5, 0x3, 0x2, 0x2, 0x2, 0x1e, 0xf7, 0x3, 0x2, 0x2, 
    0x2, 0x20, 0xfc, 0x3, 0x2, 0x2, 0x2, 0x22, 0xfe, 0x3, 0x2, 0x2, 0x2, 
    0x24, 0x109, 0x3, 0x2, 0x2, 0x2, 0x26, 0x114, 0x3, 0x2, 0x2, 0x2, 0x28, 
    0x117, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x122, 0x3, 0x2, 0x2, 0x2, 0x2c, 0x2d, 
    0x5, 0x4, 0x3, 0x2, 0x2d, 0x3, 0x3, 0x2, 0x2, 0x2, 0x2e, 0x30, 0x5, 
    0x10, 0x9, 0x2, 0x2f, 0x2e, 0x3, 0x2, 0x2, 0x2, 0x30, 0x33, 0x3, 0x2, 
    0x2, 0x2, 0x31, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x31, 0x32, 0x3, 0x2, 0x2, 
    0x2, 0x32, 0x37, 0x3, 0x2, 0x2, 0x2, 0x33, 0x31, 0x3, 0x2, 0x2, 0x2, 
    0x34, 0x36, 0x5, 0x8, 0x5, 0x2, 0x35, 0x34, 0x3, 0x2, 0x2, 0x2, 0x36, 
    0x39, 0x3, 0x2, 0x2, 0x2, 0x37, 0x35, 0x3, 0x2, 0x2, 0x2, 0x37, 0x38, 
    0x3, 0x2, 0x2, 0x2, 0x38, 0x3a, 0x3, 0x2, 0x2, 0x2, 0x39, 0x37, 0x3, 
    0x2, 0x2, 0x2, 0x3a, 0x3b, 0x5, 0x6, 0x4, 0x2, 0x3b, 0x5, 0x3, 0x2, 
    0x2, 0x2, 0x3c, 0x3d, 0x7, 0x20, 0x2, 0x2, 0x3d, 0x3e, 0x7, 0x3, 0x2, 
    0x2, 0x3e, 0x3f, 0x7, 0x4, 0x2, 0x2, 0x3f, 0x40, 0x7, 0x5, 0x2, 0x2, 
    0x40, 0x41, 0x7, 0x6, 0x2, 0x2, 0x41, 0x42, 0x5, 0xc, 0x7, 0x2, 0x42, 
    0x43, 0x7, 0x7, 0x2, 0x2, 0x43, 0x7, 0x3, 0x2, 0x2, 0x2, 0x44, 0x45, 
    0x5, 0x2a, 0x16, 0x2, 0x45, 0x46, 0x7, 0x22, 0x2, 0x2, 0x46, 0x47, 0x7, 
    0x4, 0x2, 0x2, 0x47, 0x48, 0x5, 0x24, 0x13, 0x2, 0x48, 0x49, 0x7, 0x5, 
    0x2, 0x2, 0x49, 0x4a, 0x7, 0x6, 0x2, 0x2, 0x4a, 0x4b, 0x5, 0xc, 0x7, 
    0x2, 0x4b, 0x4c, 0x7, 0x7, 0x2, 0x2, 0x4c, 0x56, 0x3, 0x2, 0x2, 0x2, 
    0x4d, 0x4e, 0x5, 0x2a, 0x16, 0x2, 0x4e, 0x4f, 0x7, 0x22, 0x2, 0x2, 0x4f, 
    0x50, 0x7, 0x4, 0x2, 0x2, 0x50, 0x51, 0x7, 0x5, 0x2, 0x2, 0x51, 0x52, 
    0x7, 0x6, 0x2, 0x2, 0x52, 0x53, 0x5, 0xc, 0x7, 0x2, 0x53, 0x54, 0x7, 
    0x7, 0x2, 0x2, 0x54, 0x56, 0x3, 0x2, 0x2, 0x2, 0x55, 0x44, 0x3, 0x2, 
    0x2, 0x2, 0x55, 0x4d, 0x3, 0x2, 0x2, 0x2, 0x56, 0x9, 0x3, 0x2, 0x2, 
    0x2, 0x57, 0x58, 0x7, 0x22, 0x2, 0x2, 0x58, 0x59, 0x7, 0x4, 0x2, 0x2, 
    0x59, 0x5a, 0x5, 0x28, 0x15, 0x2, 0x5a, 0x5b, 0x7, 0x5, 0x2, 0x2, 0x5b, 
    0x60, 0x3, 0x2, 0x2, 0x2, 0x5c, 0x5d, 0x7, 0x22, 0x2, 0x2, 0x5d, 0x5e, 
    0x7, 0x4, 0x2, 0x2, 0x5e, 0x60, 0x7, 0x5, 0x2, 0x2, 0x5f, 0x57, 0x3, 
    0x2, 0x2, 0x2, 0x5f, 0x5c, 0x3, 0x2, 0x2, 0x2, 0x60, 0xb, 0x3, 0x2, 
    0x2, 0x2, 0x61, 0x62, 0x5, 0x12, 0xa, 0x2, 0x62, 0x63, 0x5, 0xc, 0x7, 
    0x2, 0x63, 0x7d, 0x3, 0x2, 0x2, 0x2, 0x64, 0x65, 0x5, 0x14, 0xb, 0x2, 
    0x65, 0x66, 0x5, 0xc, 0x7, 0x2, 0x66, 0x7d, 0x3, 0x2, 0x2, 0x2, 0x67, 
    0x68, 0x5, 0x16, 0xc, 0x2, 0x68, 0x69, 0x5, 0xc, 0x7, 0x2, 0x69, 0x7d, 
    0x3, 0x2, 0x2, 0x2, 0x6a, 0x6b, 0x5, 0xa, 0x6, 0x2, 0x6b, 0x6c, 0x7, 
    0x8, 0x2, 0x2, 0x6c, 0x6d, 0x5, 0xc, 0x7, 0x2, 0x6d, 0x7d, 0x3, 0x2, 
    0x2, 0x2, 0x6e, 0x6f, 0x5, 0x1c, 0xf, 0x2, 0x6f, 0x70, 0x5, 0xc, 0x7, 
    0x2, 0x70, 0x7d, 0x3, 0x2, 0x2, 0x2, 0x71, 0x72, 0x5, 0x1a, 0xe, 0x2, 
    0x72, 0x73, 0x5, 0xc, 0x7, 0x2, 0x73, 0x7d, 0x3, 0x2, 0x2, 0x2, 0x74, 
    0x75, 0x5, 0xe, 0x8, 0x2, 0x75, 0x76, 0x5, 0xc, 0x7, 0x2, 0x76, 0x78, 
    0x3, 0x2, 0x2, 0x2, 0x77, 0x74, 0x3, 0x2, 0x2, 0x2, 0x78, 0x7b, 0x3, 
    0x2, 0x2, 0x2, 0x79, 0x77, 0x3, 0x2, 0x2, 0x2, 0x79, 0x7a, 0x3, 0x2, 
    0x2, 0x2, 0x7a, 0x7d, 0x3, 0x2, 0x2, 0x2, 0x7b, 0x79, 0x3, 0x2, 0x2, 
    0x2, 0x7c, 0x61, 0x3, 0x2, 0x2, 0x2, 0x7c, 0x64, 0x3, 0x2, 0x2, 0x2, 
    0x7c, 0x67, 0x3, 0x2, 0x2, 0x2, 0x7c, 0x6a, 0x3, 0x2, 0x2, 0x2, 0x7c, 
    0x6e, 0x3, 0x2, 0x2, 0x2, 0x7c, 0x71, 0x3, 0x2, 0x2, 0x2, 0x7c, 0x79, 
    0x3, 0x2, 0x2, 0x2, 0x7d, 0xd, 0x3, 0x2, 0x2, 0x2, 0x7e, 0x7f, 0x7, 
    0x21, 0x2, 0x2, 0x7f, 0x85, 0x7, 0x8, 0x2, 0x2, 0x80, 0x81, 0x7, 0x21, 
    0x2, 0x2, 0x81, 0x82, 0x5, 0x18, 0xd, 0x2, 0x82, 0x83, 0x7, 0x8, 0x2, 
    0x2, 0x83, 0x85, 0x3, 0x2, 0x2, 0x2, 0x84, 0x7e, 0x3, 0x2, 0x2, 0x2, 
    0x84, 0x80, 0x3, 0x2, 0x2, 0x2, 0x85, 0xf, 0x3, 0x2, 0x2, 0x2, 0x86, 
    0x8a, 0x7, 0x25, 0x2, 0x2, 0x87, 0x88, 0x7, 0x25, 0x2, 0x2, 0x88, 0x8a, 
    0x5, 0x10, 0x9, 0x2, 0x89, 0x86, 0x3, 0x2, 0x2, 0x2, 0x89, 0x87, 0x3, 
    0x2, 0x2, 0x2, 0x8a, 0x11, 0x3, 0x2, 0x2, 0x2, 0x8b, 0x8c, 0x7, 0x20, 
    0x2, 0x2, 0x8c, 0x8d, 0x5, 0x22, 0x12, 0x2, 0x8d, 0x8e, 0x7, 0x8, 0x2, 
    0x2, 0x8e, 0x13, 0x3, 0x2, 0x2, 0x2, 0x8f, 0x90, 0x7, 0x20, 0x2, 0x2, 
    0x90, 0x91, 0x7, 0x22, 0x2, 0x2, 0x91, 0x92, 0x7, 0x9, 0x2, 0x2, 0x92, 
    0x93, 0x5, 0x18, 0xd, 0x2, 0x93, 0x94, 0x7, 0x8, 0x2, 0x2, 0x94, 0x15, 
    0x3, 0x2, 0x2, 0x2, 0x95, 0x96, 0x7, 0x22, 0x2, 0x2, 0x96, 0x97, 0x7, 
    0x9, 0x2, 0x2, 0x97, 0x98, 0x5, 0x18, 0xd, 0x2, 0x98, 0x99, 0x7, 0x8, 
    0x2, 0x2, 0x99, 0x17, 0x3, 0x2, 0x2, 0x2, 0x9a, 0x9b, 0x8, 0xd, 0x1, 
    0x2, 0x9b, 0xa6, 0x5, 0x20, 0x11, 0x2, 0x9c, 0x9d, 0x7, 0xa, 0x2, 0x2, 
    0x9d, 0xa6, 0x5, 0x18, 0xd, 0x16, 0x9e, 0x9f, 0x7, 0xb, 0x2, 0x2, 0x9f, 
    0xa6, 0x5, 0x18, 0xd, 0x15, 0xa0, 0xa6, 0x5, 0xa, 0x6, 0x2, 0xa1, 0xa2, 
    0x7, 0x4, 0x2, 0x2, 0xa2, 0xa3, 0x5, 0x18, 0xd, 0x2, 0xa3, 0xa4, 0x7, 
    0x5, 0x2, 0x2, 0xa4, 0xa6, 0x3, 0x2, 0x2, 0x2, 0xa5, 0x9a, 0x3, 0x2, 
    0x2, 0x2, 0xa5, 0x9c, 0x3, 0x2, 0x2, 0x2, 0xa5, 0x9e, 0x3, 0x2, 0x2, 
    0x2, 0xa5, 0xa0, 0x3, 0x2, 0x2, 0x2, 0xa5, 0xa1, 0x3, 0x2, 0x2, 0x2, 
    0xa6, 0xd9, 0x3, 0x2, 0x2, 0x2, 0xa7, 0xa8, 0xc, 0x14, 0x2, 0x2, 0xa8, 
    0xa9, 0x7, 0xc, 0x2, 0x2, 0xa9, 0xd8, 0x5, 0x18, 0xd, 0x15, 0xaa, 0xab, 
    0xc, 0x13, 0x2, 0x2, 0xab, 0xac, 0x7, 0xd, 0x2, 0x2, 0xac, 0xd8, 0x5, 
    0x18, 0xd, 0x14, 0xad, 0xae, 0xc, 0x12, 0x2, 0x2, 0xae, 0xaf, 0x7, 0xe, 
    0x2, 0x2, 0xaf, 0xd8, 0x5, 0x18, 0xd, 0x13, 0xb0, 0xb1, 0xc, 0x11, 0x2, 
    0x2, 0xb1, 0xb2, 0x7, 0xf, 0x2, 0x2, 0xb2, 0xd8, 0x5, 0x18, 0xd, 0x12, 
    0xb3, 0xb4, 0xc, 0x10, 0x2, 0x2, 0xb4, 0xb5, 0x7, 0xb, 0x2, 0x2, 0xb5, 
    0xd8, 0x5, 0x18, 0xd, 0x11, 0xb6, 0xb7, 0xc, 0xf, 0x2, 0x2, 0xb7, 0xb8, 
    0x7, 0x10, 0x2, 0x2, 0xb8, 0xd8, 0x5, 0x18, 0xd, 0x10, 0xb9, 0xba, 0xc, 
    0xe, 0x2, 0x2, 0xba, 0xbb, 0x7, 0x11, 0x2, 0x2, 0xbb, 0xd8, 0x5, 0x18, 
    0xd, 0xf, 0xbc, 0xbd, 0xc, 0xd, 0x2, 0x2, 0xbd, 0xbe, 0x7, 0x12, 0x2, 
    0x2, 0xbe, 0xd8, 0x5, 0x18, 0xd, 0xe, 0xbf, 0xc0, 0xc, 0xc, 0x2, 0x2, 
    0xc0, 0xc1, 0x7, 0x13, 0x2, 0x2, 0xc1, 0xd8, 0x5, 0x18, 0xd, 0xd, 0xc2, 
    0xc3, 0xc, 0xb, 0x2, 0x2, 0xc3, 0xc4, 0x7, 0x14, 0x2, 0x2, 0xc4, 0xd8, 
    0x5, 0x18, 0xd, 0xc, 0xc5, 0xc6, 0xc, 0xa, 0x2, 0x2, 0xc6, 0xc7, 0x7, 
    0x15, 0x2, 0x2, 0xc7, 0xd8, 0x5, 0x18, 0xd, 0xb, 0xc8, 0xc9, 0xc, 0x9, 
    0x2, 0x2, 0xc9, 0xca, 0x7, 0x16, 0x2, 0x2, 0xca, 0xd8, 0x5, 0x18, 0xd, 
    0xa, 0xcb, 0xcc, 0xc, 0x8, 0x2, 0x2, 0xcc, 0xcd, 0x7, 0x17, 0x2, 0x2, 
    0xcd, 0xd8, 0x5, 0x18, 0xd, 0x9, 0xce, 0xcf, 0xc, 0x7, 0x2, 0x2, 0xcf, 
    0xd0, 0x7, 0x18, 0x2, 0x2, 0xd0, 0xd8, 0x5, 0x18, 0xd, 0x8, 0xd1, 0xd2, 
    0xc, 0x6, 0x2, 0x2, 0xd2, 0xd3, 0x7, 0x19, 0x2, 0x2, 0xd3, 0xd8, 0x5, 
    0x18, 0xd, 0x7, 0xd4, 0xd5, 0xc, 0x5, 0x2, 0x2, 0xd5, 0xd6, 0x7, 0x1a, 
    0x2, 0x2, 0xd6, 0xd8, 0x5, 0x18, 0xd, 0x6, 0xd7, 0xa7, 0x3, 0x2, 0x2, 
    0x2, 0xd7, 0xaa, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xad, 0x3, 0x2, 0x2, 0x2, 
    0xd7, 0xb0, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xb3, 0x3, 0x2, 0x2, 0x2, 0xd7, 
    0xb6, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xb9, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xbc, 
    0x3, 0x2, 0x2, 0x2, 0xd7, 0xbf, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xc2, 0x3, 
    0x2, 0x2, 0x2, 0xd7, 0xc5, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xc8, 0x3, 0x2, 
    0x2, 0x2, 0xd7, 0xcb, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xce, 0x3, 0x2, 0x2, 
    0x2, 0xd7, 0xd1, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xd4, 0x3, 0x2, 0x2, 0x2, 
    0xd8, 0xdb, 0x3, 0x2, 0x2, 0x2, 0xd9, 0xd7, 0x3, 0x2, 0x2, 0x2, 0xd9, 
    0xda, 0x3, 0x2, 0x2, 0x2, 0xda, 0x19, 0x3, 0x2, 0x2, 0x2, 0xdb, 0xd9, 
    0x3, 0x2, 0x2, 0x2, 0xdc, 0xdd, 0x7, 0x1b, 0x2, 0x2, 0xdd, 0xde, 0x7, 
    0x4, 0x2, 0x2, 0xde, 0xdf, 0x5, 0x18, 0xd, 0x2, 0xdf, 0xe0, 0x7, 0x5, 
    0x2, 0x2, 0xe0, 0xe1, 0x7, 0x6, 0x2, 0x2, 0xe1, 0xe2, 0x5, 0xc, 0x7, 
    0x2, 0xe2, 0xe3, 0x7, 0x7, 0x2, 0x2, 0xe3, 0x1b, 0x3, 0x2, 0x2, 0x2, 
    0xe4, 0xe5, 0x7, 0x1c, 0x2, 0x2, 0xe5, 0xe6, 0x7, 0x4, 0x2, 0x2, 0xe6, 
    0xe7, 0x5, 0x18, 0xd, 0x2, 0xe7, 0xe8, 0x7, 0x5, 0x2, 0x2, 0xe8, 0xe9, 
    0x7, 0x6, 0x2, 0x2, 0xe9, 0xea, 0x5, 0xc, 0x7, 0x2, 0xea, 0xeb, 0x7, 
    0x7, 0x2, 0x2, 0xeb, 0xf6, 0x3, 0x2, 0x2, 0x2, 0xec, 0xed, 0x7, 0x1c, 
    0x2, 0x2, 0xed, 0xee, 0x7, 0x4, 0x2, 0x2, 0xee, 0xef, 0x5, 0x18, 0xd, 
    0x2, 0xef, 0xf0, 0x7, 0x5, 0x2, 0x2, 0xf0, 0xf1, 0x7, 0x6, 0x2, 0x2, 
    0xf1, 0xf2, 0x5, 0xc, 0x7, 0x2, 0xf2, 0xf3, 0x7, 0x7, 0x2, 0x2, 0xf3, 
    0xf4, 0x5, 0x1e, 0x10, 0x2, 0xf4, 0xf6, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xe4, 
    0x3, 0x2, 0x2, 0x2, 0xf5, 0xec, 0x3, 0x2, 0x2, 0x2, 0xf6, 0x1d, 0x3, 
    0x2, 0x2, 0x2, 0xf7, 0xf8, 0x7, 0x1d, 0x2, 0x2, 0xf8, 0xf9, 0x7, 0x6, 
    0x2, 0x2, 0xf9, 0xfa, 0x5, 0xc, 0x7, 0x2, 0xfa, 0xfb, 0x7, 0x7, 0x2, 
    0x2, 0xfb, 0x1f, 0x3, 0x2, 0x2, 0x2, 0xfc, 0xfd, 0x9, 0x2, 0x2, 0x2, 
    0xfd, 0x21, 0x3, 0x2, 0x2, 0x2, 0xfe, 0xff, 0x8, 0x12, 0x1, 0x2, 0xff, 
    0x100, 0x7, 0x22, 0x2, 0x2, 0x100, 0x106, 0x3, 0x2, 0x2, 0x2, 0x101, 
    0x102, 0xc, 0x3, 0x2, 0x2, 0x102, 0x103, 0x7, 0x1e, 0x2, 0x2, 0x103, 
    0x105, 0x7, 0x22, 0x2, 0x2, 0x104, 0x101, 0x3, 0x2, 0x2, 0x2, 0x105, 
    0x108, 0x3, 0x2, 0x2, 0x2, 0x106, 0x104, 0x3, 0x2, 0x2, 0x2, 0x106, 
    0x107, 0x3, 0x2, 0x2, 0x2, 0x107, 0x23, 0x3, 0x2, 0x2, 0x2, 0x108, 0x106, 
    0x3, 0x2, 0x2, 0x2, 0x109, 0x10a, 0x8, 0x13, 0x1, 0x2, 0x10a, 0x10b, 
    0x5, 0x26, 0x14, 0x2, 0x10b, 0x111, 0x3, 0x2, 0x2, 0x2, 0x10c, 0x10d, 
    0xc, 0x3, 0x2, 0x2, 0x10d, 0x10e, 0x7, 0x1e, 0x2, 0x2, 0x10e, 0x110, 
    0x5, 0x26, 0x14, 0x2, 0x10f, 0x10c, 0x3, 0x2, 0x2, 0x2, 0x110, 0x113, 
    0x3, 0x2, 0x2, 0x2, 0x111, 0x10f, 0x3, 0x2, 0x2, 0x2, 0x111, 0x112, 
    0x3, 0x2, 0x2, 0x2, 0x112, 0x25, 0x3, 0x2, 0x2, 0x2, 0x113, 0x111, 0x3, 
    0x2, 0x2, 0x2, 0x114, 0x115, 0x7, 0x20, 0x2, 0x2, 0x115, 0x116, 0x7, 
    0x22, 0x2, 0x2, 0x116, 0x27, 0x3, 0x2, 0x2, 0x2, 0x117, 0x118, 0x8, 
    0x15, 0x1, 0x2, 0x118, 0x119, 0x5, 0x18, 0xd, 0x2, 0x119, 0x11f, 0x3, 
    0x2, 0x2, 0x2, 0x11a, 0x11b, 0xc, 0x3, 0x2, 0x2, 0x11b, 0x11c, 0x7, 
    0x1e, 0x2, 0x2, 0x11c, 0x11e, 0x5, 0x18, 0xd, 0x2, 0x11d, 0x11a, 0x3, 
    0x2, 0x2, 0x2, 0x11e, 0x121, 0x3, 0x2, 0x2, 0x2, 0x11f, 0x11d, 0x3, 
    0x2, 0x2, 0x2, 0x11f, 0x120, 0x3, 0x2, 0x2, 0x2, 0x120, 0x29, 0x3, 0x2, 
    0x2, 0x2, 0x121, 0x11f, 0x3, 0x2, 0x2, 0x2, 0x122, 0x123, 0x9, 0x3, 
    0x2, 0x2, 0x123, 0x2b, 0x3, 0x2, 0x2, 0x2, 0x11, 0x31, 0x37, 0x55, 0x5f, 
    0x79, 0x7c, 0x84, 0x89, 0xa5, 0xd7, 0xd9, 0xf5, 0x106, 0x111, 0x11f, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

ifccParser::Initializer ifccParser::_init;
