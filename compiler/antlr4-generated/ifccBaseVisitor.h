
// Generated from ifcc.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "ifccVisitor.h"


/**
 * This class provides an empty implementation of ifccVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  ifccBaseVisitor : public ifccVisitor {
public:

  virtual antlrcpp::Any visitAxiom(ifccParser::AxiomContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitProg(ifccParser::ProgContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMain(ifccParser::MainContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDeclarationAvecParams(ifccParser::DeclarationAvecParamsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDeclarationSansParams(ifccParser::DeclarationSansParamsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAppelAvecParams(ifccParser::AppelAvecParamsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAppelSansParams(ifccParser::AppelSansParamsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCode(ifccParser::CodeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitReturnVoid(ifccParser::ReturnVoidContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitReturnExpr(ifccParser::ReturnExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDirective(ifccParser::DirectiveContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDeclaration(ifccParser::DeclarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDefinition(ifccParser::DefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAffectation(ifccParser::AffectationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPar(ifccParser::ParContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAdd(ifccParser::AddContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSub(ifccParser::SubContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMult(ifccParser::MultContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMod(ifccParser::ModContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitOr(ifccParser::OrContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitValeur(ifccParser::ValeurContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLess(ifccParser::LessContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitOpp(ifccParser::OppContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCall(ifccParser::CallContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDiv(ifccParser::DivContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitEqual(ifccParser::EqualContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNot(ifccParser::NotContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAnd(ifccParser::AndContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLessequal(ifccParser::LessequalContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGreaterequal(ifccParser::GreaterequalContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitXor(ifccParser::XorContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGreater(ifccParser::GreaterContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNequal(ifccParser::NequalContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBoucleWhile(ifccParser::BoucleWhileContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCondition(ifccParser::ConditionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAlternative(ifccParser::AlternativeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVal(ifccParser::ValContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMultivar(ifccParser::MultivarContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParams(ifccParser::ParamsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParam(ifccParser::ParamContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParamscall(ifccParser::ParamscallContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunctionType(ifccParser::FunctionTypeContext *ctx) override {
    return visitChildren(ctx);
  }


};

