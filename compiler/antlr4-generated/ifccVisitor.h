
// Generated from ifcc.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "ifccParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by ifccParser.
 */
class  ifccVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by ifccParser.
   */
    virtual antlrcpp::Any visitAxiom(ifccParser::AxiomContext *context) = 0;

    virtual antlrcpp::Any visitProg(ifccParser::ProgContext *context) = 0;

    virtual antlrcpp::Any visitMain(ifccParser::MainContext *context) = 0;

    virtual antlrcpp::Any visitDeclarationAvecParams(ifccParser::DeclarationAvecParamsContext *context) = 0;

    virtual antlrcpp::Any visitDeclarationSansParams(ifccParser::DeclarationSansParamsContext *context) = 0;

    virtual antlrcpp::Any visitAppelAvecParams(ifccParser::AppelAvecParamsContext *context) = 0;

    virtual antlrcpp::Any visitAppelSansParams(ifccParser::AppelSansParamsContext *context) = 0;

    virtual antlrcpp::Any visitCode(ifccParser::CodeContext *context) = 0;

    virtual antlrcpp::Any visitReturnVoid(ifccParser::ReturnVoidContext *context) = 0;

    virtual antlrcpp::Any visitReturnExpr(ifccParser::ReturnExprContext *context) = 0;

    virtual antlrcpp::Any visitDirective(ifccParser::DirectiveContext *context) = 0;

    virtual antlrcpp::Any visitDeclaration(ifccParser::DeclarationContext *context) = 0;

    virtual antlrcpp::Any visitDefinition(ifccParser::DefinitionContext *context) = 0;

    virtual antlrcpp::Any visitAffectation(ifccParser::AffectationContext *context) = 0;

    virtual antlrcpp::Any visitPar(ifccParser::ParContext *context) = 0;

    virtual antlrcpp::Any visitAdd(ifccParser::AddContext *context) = 0;

    virtual antlrcpp::Any visitSub(ifccParser::SubContext *context) = 0;

    virtual antlrcpp::Any visitMult(ifccParser::MultContext *context) = 0;

    virtual antlrcpp::Any visitMod(ifccParser::ModContext *context) = 0;

    virtual antlrcpp::Any visitOr(ifccParser::OrContext *context) = 0;

    virtual antlrcpp::Any visitValeur(ifccParser::ValeurContext *context) = 0;

    virtual antlrcpp::Any visitLess(ifccParser::LessContext *context) = 0;

    virtual antlrcpp::Any visitOpp(ifccParser::OppContext *context) = 0;

    virtual antlrcpp::Any visitCall(ifccParser::CallContext *context) = 0;

    virtual antlrcpp::Any visitDiv(ifccParser::DivContext *context) = 0;

    virtual antlrcpp::Any visitEqual(ifccParser::EqualContext *context) = 0;

    virtual antlrcpp::Any visitNot(ifccParser::NotContext *context) = 0;

    virtual antlrcpp::Any visitAnd(ifccParser::AndContext *context) = 0;

    virtual antlrcpp::Any visitLessequal(ifccParser::LessequalContext *context) = 0;

    virtual antlrcpp::Any visitGreaterequal(ifccParser::GreaterequalContext *context) = 0;

    virtual antlrcpp::Any visitXor(ifccParser::XorContext *context) = 0;

    virtual antlrcpp::Any visitGreater(ifccParser::GreaterContext *context) = 0;

    virtual antlrcpp::Any visitNequal(ifccParser::NequalContext *context) = 0;

    virtual antlrcpp::Any visitBoucleWhile(ifccParser::BoucleWhileContext *context) = 0;

    virtual antlrcpp::Any visitCondition(ifccParser::ConditionContext *context) = 0;

    virtual antlrcpp::Any visitAlternative(ifccParser::AlternativeContext *context) = 0;

    virtual antlrcpp::Any visitVal(ifccParser::ValContext *context) = 0;

    virtual antlrcpp::Any visitMultivar(ifccParser::MultivarContext *context) = 0;

    virtual antlrcpp::Any visitParams(ifccParser::ParamsContext *context) = 0;

    virtual antlrcpp::Any visitParam(ifccParser::ParamContext *context) = 0;

    virtual antlrcpp::Any visitParamscall(ifccParser::ParamscallContext *context) = 0;

    virtual antlrcpp::Any visitFunctionType(ifccParser::FunctionTypeContext *context) = 0;


};

