// Copied from a file generated from ifcc.g4 by ANTLR 4.7.2


#include "visitor.h"
#include <list>
#include <utility> 

Visitor::Visitor()
{
	tableDesFonctions = TableSymbole();
	listCFG = vector<CFG*>();
	listCFG.push_back(new CFG());
	asmType = OutputType::x86;	//par defaut
}

Visitor::~Visitor()
{

}

void Visitor::gen_asm(ostream& o)
{
	switch(asmType)
	{
		case OutputType::x86:
			o<<"\t.text           # declaration of ’text’ section (which means ’program’)"<<endl;
			o<<"\t.globl main    # entry point to the ELF linker or loader."<<endl;
			break;
		case OutputType::ARM:
			break;
	}
	for(int i=0; i<listCFG.size(); i++)
	{
		listCFG[i]->gen_asm(o, asmType);
	}
}

antlrcpp::Any Visitor::visitAxiom(ifccParser::AxiomContext *ctx) 
{
	return visitChildren(ctx);
}

antlrcpp::Any Visitor::visitProg(ifccParser::ProgContext *ctx)  
{
	return visitChildren(ctx);
}

antlrcpp::Any Visitor::visitMain(ifccParser::MainContext *ctx)
{
	//cout<<"#--visitMain--"<<endl;

	CFG* funcCFG = new CFG();
	BasicBlock* bb = new BasicBlock(funcCFG, "main");
	funcCFG->add_bb(bb);
	listCFG.push_back(funcCFG);

	vector<string> params = vector<string>();

	IRInstr * instrInit = new IRInstrInit_rbp(bb, Type::INT , params);

	bb->instrs.push_back(instrInit);
	visitChildren(ctx);
	return 0;
}

antlrcpp::Any Visitor::visitCode(ifccParser::CodeContext *ctx)
{
	//cout<<"#--visitCode--"<<endl;

	return visitChildren(ctx);
}

antlrcpp::Any Visitor::visitReturnExpr(ifccParser::ReturnExprContext *ctx)
{
	//cout<<"#--visitReturnExpr--"<<endl;

	string* returnVariablePtr = visit(ctx->expression());
	CFG* cfg = listCFG[listCFG.size()-1];
	Variable* returnVariable= cfg->get_var(*returnVariablePtr);
	
	BasicBlock* bb = cfg->current_bb;
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*returnVariablePtr, asmType);
	string param2 = to_string((cfg->getNbVars())*8);
	params.push_back(param2);
	params.push_back(param1);

	IRInstr* newIRInstr = new IRInstrRet(bb, returnVariable->type, params);
	bb->instrs.push_back(newIRInstr);

	return 0;
}



antlrcpp::Any Visitor::visitReturnVoid(ifccParser::ReturnVoidContext *ctx)
{
	//cout<<"#--visitReturnVoid--"<<endl;

	CFG* cfg = listCFG[listCFG.size()-1];
	BasicBlock* bb = cfg->current_bb;
	vector<string> params = vector<string>();
	string param2 = to_string((cfg->getNbVars())*8);
	params.push_back(param2);

	IRInstr* newIRInstr = new IRInstrRet(bb, Type::INT, params);
	bb->instrs.push_back(newIRInstr);

	return 0;
}

antlrcpp::Any Visitor::visitDirective(ifccParser::DirectiveContext *ctx)
{
	//cout<<"#--visitDirective--"<<endl;

	return visitChildren(ctx);
}

antlrcpp::Any Visitor::visitDeclaration(ifccParser::DeclarationContext *ctx)
{
	//cout<<"#--visitDeclaration--"<<endl;

	string type = ctx->TYPE()->getText();
	MultiVar* variables = visitMultivar(ctx->multivar());
	if(type == "int")
	{
		variables->AddToTableSymbole(*listCFG[listCFG.size()-1], Type::INT);
	}
	return 0;
}


antlrcpp::Any Visitor::visitDefinition(ifccParser::DefinitionContext *ctx)
{
	//cout<<"#--visitDefinition--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];
	string type = ctx->TYPE()->getText();
	string nomVar = ctx->VARIABLE()->getText();
	if(type == "int")
	{
		cfg->add_to_symbol_table(nomVar, Type::INT);
		string* nomRightPtr = visit(ctx->expression());

		string nomRight = *nomRightPtr; //copy
		Variable* rightVar =  cfg->get_var(nomRight);
		Variable* leftVar =  cfg->get_var(nomVar);
		BasicBlock* bb = cfg->current_bb;
		vector<string> params = vector<string>();
		string param1 = cfg->IR_reg_to_asm(nomVar, asmType);
		string param2 = cfg->IR_reg_to_asm(nomRight, asmType);
		params.push_back(param1);
		params.push_back(param2);

		IRInstr* newIRInstr = new IRInstrCopy(bb, leftVar->type, params);
		bb->instrs.push_back(newIRInstr);	
	}
	return 0;
}

antlrcpp::Any Visitor::visitVal(ifccParser::ValContext *ctx)
{
	//cout<<"#--visitVal--"<<endl;

	string* nom = new string();
	
	CFG* cfg = listCFG[listCFG.size()-1];
	if (ctx->CONST()){	//si c'est une valeur constante

		int value = stoi(ctx->CONST()->getText());
		*nom = cfg->create_new_tempvar(Type::INT);
		Variable * leftVar = cfg->get_var(*nom);

		BasicBlock* bb = cfg->current_bb;
		vector<string> params = vector<string>();
		string param1 = cfg->IR_reg_to_asm(*nom, asmType);
		params.push_back(param1);
		params.push_back(to_string(value));
		IRInstr* newIRInstr = new IRInstrLdconst(bb, leftVar->type, params);
		bb->instrs.push_back(newIRInstr);

	}else{				// sinon c'est une valeur sous forme de variable, avec son nom
		*nom = ctx->VARIABLE()->getText();
	}

	return nom;
}



antlrcpp::Any Visitor::visitParamscall(ifccParser::ParamscallContext *ctx)
{
	//cout<<"#--visitParamscall--"<<endl;

	MultiVar* vars = new MultiVar();
	readParamscall(vars, ctx);
	return vars;
}


antlrcpp::Any Visitor::visitMultivar(ifccParser::MultivarContext *ctx)
{
	//cout<<"#--visitMultivar--"<<endl;

	MultiVar* vars = new MultiVar();
	readMultiVar(vars, ctx);
	return vars;
}


antlrcpp::Any Visitor::visitAffectation(ifccParser::AffectationContext *ctx) 
{
	//cout<<"#--visitAffectation--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

    string nomLeft = ctx->VARIABLE()->getText();
	string* nomRight = visit(ctx->expression());	//visit children retourne un ptr sur le nom de la vairable temporaire

	Variable* rightVar = cfg->get_var(*nomRight);
	Variable* leftVar = cfg->get_var(nomLeft);

	BasicBlock* bb = cfg->current_bb;

	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(nomLeft, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomRight, asmType);
	params.push_back(param1);
	params.push_back(param2);

	IRInstr* newIRInstr = new IRInstrCopy(bb, leftVar->type, params);
	bb->instrs.push_back(newIRInstr);

	return 0;
}

antlrcpp::Any Visitor::visitAdd(ifccParser::AddContext *ctx)
{
	//cout<<"#--visitAdd--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	string* nomTotal = new string(cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar = cfg->get_var(*nomLeft);
	Variable* rightVar = cfg->get_var(*nomRight);
	Variable* totalVar = cfg->get_var(*nomTotal);

	BasicBlock* bb = cfg->current_bb;
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomRight, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newIRInstr = new IRInstrAdd(bb, totalVar->type, params);
	bb->instrs.push_back(newIRInstr);

	return nomTotal;
}

antlrcpp::Any Visitor::visitSub(ifccParser::SubContext *ctx)
{
	//cout<<"#--visitSub--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	string* nomTotal = new string(cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar = cfg->get_var(*nomLeft);
	Variable* rightVar = cfg->get_var(*nomRight);
	Variable* totalVar = cfg->get_var(*nomTotal);

	BasicBlock* bb = cfg->current_bb;
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomRight, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newIRInstr = new IRInstrSub(bb, totalVar->type, params);

	bb->instrs.push_back(newIRInstr);

	return nomTotal;
}

antlrcpp::Any Visitor::visitMult(ifccParser::MultContext *ctx)
{
	//cout<<"#--visitMult--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	string* nomTotal = new string(cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar = cfg->get_var(*nomLeft);
	Variable* rightVar = cfg->get_var(*nomRight);
	Variable* totalVar = cfg->get_var(*nomTotal);

	BasicBlock* bb = cfg->current_bb;
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomRight, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newIRInstr = new IRInstrMul(bb, totalVar->type, params);

	bb->instrs.push_back(newIRInstr);

	return nomTotal;
}

antlrcpp::Any Visitor::visitDiv(ifccParser::DivContext *ctx)
{
	//cout<<"#--visitDiv--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	string* nomTotal = new string(cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar = cfg->get_var(*nomLeft);
	Variable* rightVar = cfg->get_var(*nomRight);
	Variable* totalVar = cfg->get_var(*nomTotal);

	BasicBlock* bb = cfg->current_bb;
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomRight, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newIRInstr = new IRInstrDiv(bb, totalVar->type, params);
	bb->instrs.push_back(newIRInstr);

	return nomTotal;
}

antlrcpp::Any Visitor::visitMod(ifccParser::ModContext *ctx)
{
	//cout<<"#--visitMod--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	string* nomTotal = new string(cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar = cfg->get_var(*nomLeft);
	Variable* rightVar = cfg->get_var(*nomRight);
	Variable* totalVar = cfg->get_var(*nomTotal);

	BasicBlock* bb = cfg->current_bb;
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomRight, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newIRInstr = new IRInstrMod(bb, totalVar->type, params);
	bb->instrs.push_back(newIRInstr);

	return nomTotal;
}

antlrcpp::Any Visitor::visitOr(ifccParser::OrContext *ctx){
	//cout<<"#--visitOr--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	string* nomTotal = new string(cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar = cfg->get_var(*nomLeft);
	Variable* rightVar = cfg->get_var(*nomRight);
	Variable* totalVar = cfg->get_var(*nomTotal);

	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomRight, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newInstr = new IRInstrOr(cfg->current_bb, Type::INT, params);
	cfg->current_bb->instrs.push_back(newInstr);

	return nomTotal;
};

antlrcpp::Any Visitor::visitXor(ifccParser::XorContext *ctx){
	//cout<<"#--visitXor--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	string* nomTotal = new string(cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar = cfg->get_var(*nomLeft);
	Variable* rightVar = cfg->get_var(*nomRight);
	Variable* totalVar = cfg->get_var(*nomTotal);
	
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomRight, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newIRInstr = new IRInstrXor(cfg->current_bb, Type::INT, params);
	cfg->current_bb->instrs.push_back(newIRInstr);

	return nomTotal;
};

antlrcpp::Any Visitor::visitAnd(ifccParser::AndContext *ctx) {
	//cout<<"#--visitAnd--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	string* nomTotal = new string( cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar =  cfg->get_var(*nomLeft);
	Variable* rightVar =  cfg->get_var(*nomRight);
	Variable* totalVar =  cfg->get_var(*nomTotal);

	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomRight, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newIRInstr = new IRInstrAnd( cfg->current_bb, Type::INT, params);
	 cfg->current_bb->instrs.push_back(newIRInstr);
	
	return nomTotal;
};

antlrcpp::Any Visitor::visitValeur(ifccParser::ValeurContext *ctx)
{
	//cout<<"#--visitValeur--"<<endl;
	return visit(ctx->val()); 
}

antlrcpp::Any Visitor::visitPar(ifccParser::ParContext *ctx)
{
	//cout<<"#--visitPar--"<<endl;
	return visit(ctx->expression()); //on renvoit le nom de la variable temporaire correspondante à l'expression entre parenthèses vu qu'on la modifie pas
}

antlrcpp::Any Visitor::visitOpp(ifccParser::OppContext *ctx)
{
	//cout<<"#--visitOpp--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomRight = visit(ctx->expression());	//partie droite de l'expression
	string* nomTotal = new string( cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total

	Variable* rightVar =  cfg->get_var(*nomRight);
	Variable* totalVar =  cfg->get_var(*nomTotal);

	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomRight, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	params.push_back(param1);
	params.push_back(param2);

	IRInstr* newIRInstr = new IRInstrNeg(cfg->current_bb, Type::INT, params);
	 cfg->current_bb->instrs.push_back(newIRInstr);

	return nomTotal;
}

antlrcpp::Any Visitor::visitNot(ifccParser::NotContext *ctx)
{
	//cout<<"#--visitNot--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomRight = visit(ctx->expression());	//partie droite de l'expression
	string* nomTotal = new string( cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total

	Variable* rightVar =  cfg->get_var(*nomRight);
	Variable* totalVar =  cfg->get_var(*nomTotal);

	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomRight, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	params.push_back(param1);
	params.push_back(param2);

	IRInstr* newIRInstr = new IRInstrNot(cfg->current_bb, Type::INT, params);
	 cfg->current_bb->instrs.push_back(newIRInstr);

	return nomTotal;
}

antlrcpp::Any Visitor::visitFunctionType(ifccParser::FunctionTypeContext *ctx) 
{
	string retString;
	if(ctx->TYPE())
	{
		retString = ctx->TYPE()->getText();
	}
	else
	{
		retString = "void";
	}

	if(retString == "int")
	{
		return new ReturnType(ReturnType::R_INT);
	}
	if(retString == "char")
	{
		return new ReturnType(ReturnType::R_CHAR);
	}
	if(retString == "void")
	{
		return new ReturnType(ReturnType::R_VOID);
	}
	return new ReturnType(ReturnType::R_VOID); //default case
}

antlrcpp::Any Visitor::visitDeclarationSansParams(ifccParser::DeclarationSansParamsContext *ctx) 
{
	//cout<<"#--visitDeclarationSansParams--"<<endl;
	ReturnType* retType = visit(ctx->functionType());
	string nomFunc = ctx->VARIABLE()->getText();
	if(containsFunctionBB(nomFunc))
	{
		cerr<<"Error: Function already defined !"<<endl;
	}
	else{

		tableDesFonctions.AddFunction(nomFunc, *retType, vector<Type>());

		CFG* funcCFG = new CFG();
		BasicBlock* bb = new BasicBlock(funcCFG, nomFunc, tableDesFonctions);

		funcCFG->add_bb(bb);
		listCFG.push_back(funcCFG);

		vector<string> params = vector<string>();

		IRInstr * newIRInstr = new IRInstrInit_rbp(bb, Type::INT , params);


		bb->instrs.push_back(newIRInstr);
	}
	return visitChildren(ctx);
}

antlrcpp::Any Visitor::visitDeclarationAvecParams(ifccParser::DeclarationAvecParamsContext *ctx) 
{
	//cout<<"#--visitDeclarationAvecParams--"<<endl;
	ReturnType * retType = visit(ctx->functionType());
	string nomFunc = ctx->VARIABLE()->getText();
	if(containsFunctionBB(nomFunc))
	{
		cerr<<"Error: Function already defined !"<<endl;
	}
	else{
		vector<Parametre*> * parametres = visitParams(ctx->params());
		vector<Type> toBeCalledParams = vector<Type>();
		for(auto it=parametres->begin(); it!=parametres->end(); ++it){
			toBeCalledParams.push_back((*it)->type);
		}
		tableDesFonctions.AddFunction(nomFunc, *retType, toBeCalledParams);	//ajoute une fonction qui return retType sans parametre

		CFG* funcCFG = new CFG();
		BasicBlock* bb = new BasicBlock(funcCFG, nomFunc, tableDesFonctions);
		funcCFG->add_bb(bb);

		vector<Parametre*>::iterator it;

		vector<string> params = vector<string>();
		string nomParam,param;

		for(it=parametres->begin(); it!=parametres->end(); ++it)
		{

			funcCFG->add_to_symbol_table((*it)->nom, (*it)->type);
			nomParam = (*it)->nom;
			param = funcCFG->IR_reg_to_asm(nomParam, asmType);
			params.push_back(param);
		}
		listCFG.push_back(funcCFG);
		IRInstr * instrInit = new IRInstrInit_rbp(bb, Type::INT , params);
		bb->instrs.push_back(instrInit);
	}
	return visitChildren(ctx);
}


antlrcpp::Any Visitor::visitAppelAvecParams(ifccParser::AppelAvecParamsContext *ctx) 
{
	//cout<<"#--visitAppelAvecParams--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string label = ctx->VARIABLE()->getText();
	string * label_ptr = new string(label);
	string nomTempVar = listCFG[listCFG.size()-1]->create_new_tempvar(Type::INT);
	Variable* retour =  listCFG[listCFG.size()-1]->get_var(nomTempVar);
	MultiVar* variables = visitParamscall(ctx->paramscall());
	list<string> nomsVars = variables->getNomsVars();
	//comparer avec le vector<Type> de la fonction 
	//cfg.currentBB.tableSymbole.GetFunc(nom);
	Function * func = tableDesFonctions.GetFunction(label);

	if( not(tableDesFonctions.functionExist(label)))
	{
		cerr <<"ERREUR: function " << label <<" n'existe pas."<<endl;
		exit(-1);
	}

	if (func->params.size() !=  variables->getNomsVars().size())
	{
		switch(variables->getNomsVars().size()) 
		{
			case 0 : cerr <<"ERROR: function " << label <<" nécessite " << func->params.size() << " paramètres alors qu'aucun ne lui est donné."<<endl;
			case 1 : cerr <<"ERROR: function " << label <<" nécessite " << func->params.size() << " paramètres alors que 1 seul lui est donné."<<endl;
			default : cerr <<"ERROR: function " << label <<" nécessite " << func->params.size() << " paramètres alors que "<<  variables->getNomsVars().size() <<" lui sont donné."<<endl;
		}
		exit(-1);
	}

	vector<string> params = vector<string>();
	string param0 = cfg->IR_reg_to_asm(nomTempVar, asmType);
	params.push_back(param0);
	params.push_back(label);
    list<string>::iterator it;  
	string nomParam,param;

    for(it = nomsVars.begin(); it!=nomsVars.end(); ++it)
    {
		nomParam = *it;
		param = cfg->IR_reg_to_asm(nomParam, asmType);
        params.push_back(param);  
    }

	IRInstr* newIRInstr = new IRInstrCall( cfg->current_bb, Type::INT, params);
	 cfg->current_bb->instrs.push_back(newIRInstr);

	string * nomRetour = new string(nomTempVar);
	pair<string*, string*> * label_var = new pair<string*, string*>(label_ptr, nomRetour);
	return label_var;
}

antlrcpp::Any Visitor::visitAppelSansParams(ifccParser::AppelSansParamsContext *ctx) 
{
	//cout<<"#--visitAppelSansParams--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string label = ctx->VARIABLE()->getText();	
	string * label_ptr = new string(label);
	string nomTempVar = listCFG[listCFG.size()-1]->create_new_tempvar(Type::INT);
	if( not(tableDesFonctions.functionExist(label)))
	{
		cerr <<"ERREUR: function " << label <<" n'existe pas."<<endl;
		exit(-1);
	}
	Function * func = tableDesFonctions.GetFunction(label);
	if (func->params.size() !=0)
	{
		cerr <<"ERREUR: function " << label <<" nécessite " << func->params.size() << " paramètres alors qu'aucun ne lui est donné."<<endl;
		exit(-1);
	}
	//comparer que le vector<Type> de la fonction est de taille 0

	Variable* retour =  listCFG[listCFG.size()-1]->get_var(nomTempVar);

	vector<string> params = vector<string>();
	string param0 = cfg->IR_reg_to_asm(nomTempVar, asmType);
	params.push_back(param0);
	params.push_back(label);
	IRInstr* instrCall = new IRInstrCall(cfg->current_bb, Type::INT, params);
	cfg->current_bb->instrs.push_back(instrCall);

	string * nomRetour = new string(nomTempVar);
	pair<string*, string*> * label_var = new pair<string*, string*>(label_ptr, nomRetour);
	return label_var;
}


antlrcpp::Any Visitor::visitParam(ifccParser::ParamContext *ctx) 
{
	//cout<<"#--visitParam--"<<endl;

	Parametre * param = new Parametre();
	if( ctx->TYPE()->getText() == "int") 
	{
		param->type = Type::INT;
	}
	string nom = ctx->VARIABLE()->getText();
	param->nom = nom;
	return param;
}

antlrcpp::Any Visitor::visitParams(ifccParser::ParamsContext *ctx) 
{
	//cout<<"#--visitParams--"<<endl;

	vector<Parametre*> *parametres = new vector<Parametre*>();
	readParams(parametres, ctx);
	return parametres;
}

antlrcpp::Any Visitor::visitBoucleWhile(ifccParser::BoucleWhileContext *ctx) 
{
	//cout<<"#--visitBoucleWhile--"<<endl;
	
	BasicBlock* beforeWhileBB = listCFG[listCFG.size()-1]->current_bb; //le pointeur vers le bb actuel

	BasicBlock* evaluation_BB = new BasicBlock(listCFG[listCFG.size()-1], listCFG[listCFG.size()-1]->new_BB_name(), *(beforeWhileBB->tableSymbole));
	listCFG[listCFG.size()-1]->current_bb = evaluation_BB;
	string* test = visit(ctx->expression());	//ajout de la réévaluation de la condition dans le bloc d'évaluation (pour pas avoir de boucle infinie)
	TableSymbole* tableEvaBB = evaluation_BB->tableSymbole;
	
	//la table de symbole du while est basée sur celle du block de check de la condition du while
	BasicBlock* while_BB = new BasicBlock(listCFG[listCFG.size()-1], listCFG[listCFG.size()-1]->new_BB_name(), *(tableEvaBB));
	while_BB->exit_true = evaluation_BB; //on fait un unconditionnal jump vers l'évaluation qd le bloc du while est fini

	//la table de symbole du "apreswhile" est basée sur celle du block qui precède le while (et pas celle du while, ni de la condition)
	BasicBlock* reste_du_code_BB = new BasicBlock(listCFG[listCFG.size()-1], listCFG[listCFG.size()-1]->new_BB_name(), *(beforeWhileBB->tableSymbole));

	evaluation_BB->test_var_name = *test; //on met la bonne variable de test
	evaluation_BB->exit_true = while_BB; //si l'expression est verifiée, on rentre dans le while
	evaluation_BB->exit_false = reste_du_code_BB; //sinon on va vers le reste du code

	beforeWhileBB->exit_true = evaluation_BB;//on fait un unconditional jump vers le bloc d'évaluation

	listCFG[listCFG.size()-1]->add_bb(while_BB); //d'abord on ajoute le bloc du while
	visit(ctx->code());

	listCFG[listCFG.size()-1]->add_bb(evaluation_BB);//ensuite on ajoute le bloc de l'évaluation (cet ordre est primordial!)

	listCFG[listCFG.size()-1]->add_bb(reste_du_code_BB);//ensuite le reste du code

	return 0;
}

antlrcpp::Any Visitor::visitCondition(ifccParser::ConditionContext *ctx) 
{
	//cout<<"#--visitCondition--"<<endl;

	string* test = visit(ctx->expression());
	listCFG[listCFG.size()-1]->current_bb->test_var_name = *test;
	//Variable* test_var = listCFG[listCFG.size()-1]->get_var(listCFG[listCFG.size()-1]->current_bb->test_var_name);

	BasicBlock* current_BB = listCFG[listCFG.size()-1]->current_bb; //le pointeur vers le bb actuel

	BasicBlock* reste_du_code_BB = new BasicBlock(listCFG[listCFG.size()-1], listCFG[listCFG.size()-1]->new_BB_name(), *(current_BB->tableSymbole)); //on crée un bb pour le code apres le if/else
	current_BB->exit_false = reste_du_code_BB; //pour l'instant on ne sait pas s'il y a un else, donc le exit_false c le reste du code
	BasicBlock* exit_true_BB = new BasicBlock(listCFG[listCFG.size()-1], listCFG[listCFG.size()-1]->new_BB_name(), *(current_BB->tableSymbole)); //on crée un bb pour le cas true
	
	current_BB->exit_true = exit_true_BB; //on met les pointeurs au bon endroit
	exit_true_BB->exit_true = reste_du_code_BB; //a la fin du if on jump (unconditionnel) au reste du code
	listCFG[listCFG.size()-1]->add_bb(exit_true_BB); //on ajoute les bb a la liste des bb du cfg
	listCFG[listCFG.size()-1]->current_bb = exit_true_BB; //on met le current_bb du cfg au bb_true pour visiter son code

	visit(ctx->code()); //on visit le code du exit_true (le bloc correspondant a l'expression evaluee a true)

	BasicBlock* else_BB; //on declare un bb pour le cas ou il y a un else

	if(ctx->alternative()){// s'il y a un else
		else_BB = new BasicBlock(listCFG[listCFG.size()-1], listCFG[listCFG.size()-1]->new_BB_name(), *(current_BB->tableSymbole)); //on crée le bb du else
		current_BB->exit_false = else_BB; //vu qu'il y a un else il devient le exit_false du bb précedent
		else_BB->exit_true = reste_du_code_BB; //a la fin du else on jump (unconditionnel) au reste du code
		listCFG[listCFG.size()-1]->add_bb(else_BB);//on l'ajoute a la liste de bb
		listCFG[listCFG.size()-1]->current_bb = else_BB; //on met le current_bb du cfg au bb_else pour visiter son code
		visit(ctx->alternative());//on visite le code du else_bb (le bloc correspondant au else)
	}
	listCFG[listCFG.size()-1]->add_bb(reste_du_code_BB); //on ajoute le reste du code en dernier pour respecter l'ordre
	listCFG[listCFG.size()-1]->current_bb = reste_du_code_BB; //le code qui vient après le if/else est le reste du code, donc on met le current dessus

	return 0;
}

antlrcpp::Any Visitor::visitAlternative(ifccParser::AlternativeContext *ctx)
{
	//cout<<"#--visitAlternative--"<<endl;

	return visit(ctx->code());
}


antlrcpp::Any Visitor::visitCall(ifccParser::CallContext *ctx) 
{
	//cout<<"#--visitCall--"<<endl;
	pair<string *, string *> * func_var = visit(ctx->functioncall());
	string* nomvar = func_var->second;
	string* label = func_var->first;

	Function* func = tableDesFonctions.GetFunction(*label);

	if(func->returnType == ReturnType::R_VOID){
		cerr<<"ERREUR: Cette fonction est utilisée dans une expression alors qu'elle return void !"<<endl;
		exit(-1);
	}

	return nomvar;
}


antlrcpp::Any Visitor::visitEqual(ifccParser::EqualContext *ctx)
{
	//cout<< "#--visitEqual--" << endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	Variable* leftVar =  cfg->get_var(*nomLeft);
	Variable* rightVar =  cfg->get_var(*nomRight);
	string* nomBool = new string(cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le booleen
	Variable* varBool =  cfg->get_var(*nomBool);
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomBool, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomRight, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* instrEq = new IRInstrCmpEq( cfg->current_bb, Type::INT, params);
	 cfg->current_bb->instrs.push_back(instrEq);
	return nomBool;
};

antlrcpp::Any Visitor::visitNequal(ifccParser::NequalContext *ctx) 
{
	//cout<< "#--visitNequal--" << endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	Variable* leftVar =  cfg->get_var(*nomLeft);
	Variable* rightVar =  cfg->get_var(*nomRight);
	string* nomBool = new string( cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le booleen
	Variable* varBool =  cfg->get_var(*nomBool);
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomBool, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomRight, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* instrNeq = new IRInstrCmpNeq( cfg->current_bb, Type::INT, params);
	 cfg->current_bb->instrs.push_back(instrNeq);
	return nomBool;
};

antlrcpp::Any Visitor::visitGreater(ifccParser::GreaterContext *ctx)
{
	//cout<<"#--visitGreater--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	//inverser 0 et 1 (a>b equivalent à b<a)

	string* nomLeft = visit(ctx->expression(1));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(0));	//partie droite de l'expression
	string* nomTotal = new string( cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar =  cfg->get_var(*nomLeft);
	Variable* rightVar =  cfg->get_var(*nomRight);
	Variable* totalVar =  cfg->get_var(*nomTotal);

	BasicBlock* bb = cfg->current_bb;
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomRight, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newIRInstr = new IRInstrCmplt(bb, totalVar->type, params);

	bb->instrs.push_back(newIRInstr);
	return nomTotal;
}

antlrcpp::Any Visitor::visitLess(ifccParser::LessContext *ctx) 
{
	//cout<<"#--visitLess--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	string* nomTotal = new string( cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar =  cfg->get_var(*nomLeft);
	Variable* rightVar =  cfg->get_var(*nomRight);
	Variable* totalVar =  cfg->get_var(*nomTotal);

	BasicBlock* bb = cfg->current_bb;
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomRight, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newIRInstr = new IRInstrCmplt(bb, totalVar->type, params);

	bb->instrs.push_back(newIRInstr);
	return nomTotal;
}

	antlrcpp::Any Visitor::visitGreaterequal(ifccParser::GreaterequalContext *ctx)
{
	//cout<<"#--visitGreaterequal--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	//inverser 0 et 1 (a>b equivalent à b<a)

	string* nomLeft = visit(ctx->expression(1));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(0));	//partie droite de l'expression
	string* nomTotal = new string( cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar =  cfg->get_var(*nomLeft);
	Variable* rightVar =  cfg->get_var(*nomRight);
	Variable* totalVar =  cfg->get_var(*nomTotal);

	BasicBlock* bb = cfg->current_bb;
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomRight, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newIRInstr = new IRInstrCmple(bb, totalVar->type, params);

	bb->instrs.push_back(newIRInstr);
	return nomTotal;
}

	antlrcpp::Any Visitor::visitLessequal(ifccParser::LessequalContext *ctx)
{
	//cout<<"#--visitLessequal--"<<endl;
	CFG* cfg = listCFG[listCFG.size()-1];

	string* nomLeft = visit(ctx->expression(0));	//partie gauche de l'expression
	string* nomRight = visit(ctx->expression(1));	//partie droite de l'expression
	string* nomTotal = new string( cfg->create_new_tempvar(Type::INT)); //on crée une variable temporaire qui va contenir le total
	Variable* leftVar =  cfg->get_var(*nomLeft);
	Variable* rightVar =  cfg->get_var(*nomRight);
	Variable* totalVar =  cfg->get_var(*nomTotal);

	BasicBlock* bb = cfg->current_bb;
	vector<string> params = vector<string>();
	string param1 = cfg->IR_reg_to_asm(*nomTotal, asmType);
	string param2 = cfg->IR_reg_to_asm(*nomRight, asmType);
	string param3 = cfg->IR_reg_to_asm(*nomLeft, asmType);
	params.push_back(param1);
	params.push_back(param2);
	params.push_back(param3);

	IRInstr* newIRInstr = new IRInstrCmple(bb, totalVar->type, params);

	bb->instrs.push_back(newIRInstr);
	return nomTotal;
}

void Visitor::readParams(vector<Parametre*> * parametres, ifccParser::ParamsContext *ctx)
{
	//cout<<"#reading Params for function definition/declaration" << endl;
	Parametre *parametre = visit(ctx->param());

	if(ctx->params())
	{
		readParams(parametres, ctx->params());
	}

	parametres->push_back(parametre);
}

void Visitor::readParamscall(MultiVar* vars, ifccParser::ParamscallContext *ctx)
{
	//cout<<"#reading ParamsCall"<<endl;
	string * nomVar = visit(ctx->expression());
	if(ctx->paramscall())
	{
		readParamscall(vars, ctx->paramscall());
	}
	vars->AddNom(*nomVar);
}

void Visitor::readMultiVar(MultiVar* vars, ifccParser::MultivarContext *ctx)
{
	//cout<<"#reading multivar"<<endl;
	string nomVar = ctx->VARIABLE()->getText();
	if(ctx->multivar())
	{
		readMultiVar(vars, ctx->multivar());
	}
	vars->AddNom(nomVar);
}


bool Visitor::containsFunctionBB(string functionName)
{
	for(int i=0; i<listCFG.size(); i++)
	{
		if(listCFG[i]->containsBBWithLabel(functionName))
		{
			return true;
		}
	}
	return false;
}