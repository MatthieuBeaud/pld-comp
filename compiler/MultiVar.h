#include "IR.h"
#include <list>
#include <string>

using namespace std;

class MultiVar
{
    public: 
        MultiVar();
        ~MultiVar();

        //ajoute un nom à la liste des noms de variables de la multivar
        inline void AddNom(string nom) { nomsVars.push_back(nom); }
        
        //retourne le nombre de variables présents dans la multivar
        inline int Size() { return nomsVars.size(); }

        //affiche l'ensemble des noms des variables de la multivar
		void DisplayVars();

        //ajoute l'ensemble des variables de la multivar à la table de symbole ts, avec le type t
		void AddToTableSymbole(CFG& cfg, Type t);

        //renvoie la liste des noms de variables   
        list<string> getNomsVars()const{
            return nomsVars;
        }

    private:
        list<string> nomsVars;

};