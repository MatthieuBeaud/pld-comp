#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

#include "antlr4-runtime.h"
#include "antlr4-generated/ifccLexer.h"
#include "antlr4-generated/ifccParser.h"
#include "antlr4-generated/ifccBaseVisitor.h"
#include "visitor.h"

using namespace antlr4;
using namespace std;

int main(int argn, const char **argv) {
  stringstream in;

  if (argn>=2) {
     ifstream lecture(argv[1]);
     in << lecture.rdbuf();
  }

  OutputType outputType = OutputType::x86;
  if(argn ==3)
  {
    string param = string(argv[2]);
    if(param=="ARM" || param=="arm")
    {
      outputType = OutputType::ARM;
    }
  }

  ANTLRInputStream input(in.str());

  ifccLexer lexer(&input);

  CommonTokenStream tokens(&lexer);
 
  tokens.fill();
//  for (auto token : tokens.getTokens()) {
//    std::cout << token->toString() << std::endl;
//  }

  ifccParser parser(&tokens);

  tree::ParseTree* tree = parser.axiom();
 
  if(parser.getNumberOfSyntaxErrors() != 0)
  {
    cout<<"erreur de syntaxe"<<endl;
    return -1;
  }

  Visitor visitor;
  visitor.setOutputType(outputType);

  visitor.visit(tree);

  cout<<"#### Assembleur produit en "<<((outputType==OutputType::ARM)?"ARM":"x86")<<" ####"<<endl;
  visitor.gen_asm(cout);

  return 0;
}
