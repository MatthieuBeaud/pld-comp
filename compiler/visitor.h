
// Copied from a file generated from ifcc.g4 by ANTLR 4.7.2

#pragma once

#include <stack>
#include "antlr4-runtime.h"
#include "antlr4-generated/ifccBaseVisitor.h"
#include "TableSymbole.h"
#include "MultiVar.h"
#include "IR.h"


using namespace std;

/**
 * This class provides an empty implementation of ifccVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  Visitor : public ifccBaseVisitor {
public:

	Visitor();
	virtual ~Visitor();

	inline void setOutputType(OutputType t= OutputType::x86)
	{
		this->asmType = t;
	}

	void gen_asm(ostream& o);

	//visiteurs généraux
	virtual antlrcpp::Any visitAxiom(ifccParser::AxiomContext *ctx) override;
	virtual antlrcpp::Any visitProg(ifccParser::ProgContext *ctx) override;
    
	//visiteur directive (#include etc...)
	virtual antlrcpp::Any visitDirective(ifccParser::DirectiveContext *ctx) override;

	//visiteur main
	virtual antlrcpp::Any visitMain(ifccParser::MainContext *ctx) override;

	//visiteurs liés aux fonctions
	virtual antlrcpp::Any visitFunctionType(ifccParser::FunctionTypeContext *ctx) override;

	virtual antlrcpp::Any visitDeclarationAvecParams(ifccParser::DeclarationAvecParamsContext *ctx) override;
	virtual antlrcpp::Any visitDeclarationSansParams(ifccParser::DeclarationSansParamsContext *ctx) override;

	virtual antlrcpp::Any visitAppelAvecParams(ifccParser::AppelAvecParamsContext *ctx) override;
	virtual antlrcpp::Any visitAppelSansParams(ifccParser::AppelSansParamsContext *ctx) override;
	virtual antlrcpp::Any visitParam(ifccParser::ParamContext *ctx) override;
	virtual antlrcpp::Any visitParams(ifccParser::ParamsContext *ctx) override;
	virtual antlrcpp::Any visitParamscall(ifccParser::ParamscallContext *ctx) override;

	//visiteurs liés au code et aux returns
	virtual antlrcpp::Any visitCode(ifccParser::CodeContext *ctx) override;
	virtual antlrcpp::Any visitReturnVoid(ifccParser::ReturnVoidContext *ctx) override;
	virtual antlrcpp::Any visitReturnExpr(ifccParser::ReturnExprContext *ctx) override;
	
	//visiteurs liés aux définitions et déclarations et affectations
    virtual antlrcpp::Any visitDeclaration(ifccParser::DeclarationContext *ctx) override;
	virtual antlrcpp::Any visitDefinition(ifccParser::DefinitionContext *ctx) override;
    virtual antlrcpp::Any visitMultivar(ifccParser::MultivarContext *ctx) override;
	virtual antlrcpp::Any visitAffectation(ifccParser::AffectationContext *ctx) override;
    virtual antlrcpp::Any visitVal(ifccParser::ValContext *ctx) override;

	//visiteurs pour les expressions
	virtual antlrcpp::Any visitValeur(ifccParser::ValeurContext *ctx) override;
	//négation & opposé
    virtual antlrcpp::Any visitNot(ifccParser::NotContext *ctx) override;
    virtual antlrcpp::Any visitOpp(ifccParser::OppContext *ctx) override;
	//opérateurs * / + - et %
	virtual antlrcpp::Any visitMult(ifccParser::MultContext *ctx) override;
    virtual antlrcpp::Any visitDiv(ifccParser::DivContext *ctx) override;
    virtual antlrcpp::Any visitMod(ifccParser::ModContext *ctx) override;
	virtual antlrcpp::Any visitAdd(ifccParser::AddContext *ctx) override;
	virtual antlrcpp::Any visitSub(ifccParser::SubContext *ctx) override;
	//opérateurs bit à bit et  && et || logiques (meme fonctionnement)
    virtual antlrcpp::Any visitAnd(ifccParser::AndContext *ctx) override;
	virtual antlrcpp::Any visitOr(ifccParser::OrContext *ctx) override;
    virtual antlrcpp::Any visitXor(ifccParser::XorContext *ctx) override;
	//opérateurs de comparaison ==, !=, >, <, <=, >=
	virtual antlrcpp::Any visitEqual(ifccParser::EqualContext *ctx) override;
	virtual antlrcpp::Any visitNequal(ifccParser::NequalContext *ctx) override;
	virtual antlrcpp::Any visitGreater(ifccParser::GreaterContext *ctx) override;
	virtual antlrcpp::Any visitLess(ifccParser::LessContext *ctx) override;
	virtual antlrcpp::Any visitGreaterequal(ifccParser::GreaterequalContext *ctx) override;
	virtual antlrcpp::Any visitLessequal(ifccParser::LessequalContext *ctx) override;
	//call the fonction
	virtual antlrcpp::Any visitCall(ifccParser::CallContext *ctx) override;
	//expression entre parenthèses
	virtual antlrcpp::Any visitPar(ifccParser::ParContext *ctx) override;

	//visiteurs pour les while, if, else
	virtual antlrcpp::Any visitBoucleWhile(ifccParser::BoucleWhileContext *ctx) override;
	virtual antlrcpp::Any visitCondition(ifccParser::ConditionContext *ctx) override;
	virtual antlrcpp::Any visitAlternative(ifccParser::AlternativeContext *ctx) override;

	;

	private: 
		TableSymbole tableDesFonctions;
		vector<CFG*> listCFG;
		OutputType asmType;
		void readMultiVar(MultiVar* vars, ifccParser::MultivarContext *ctx);
		void readParamscall(MultiVar* vars, ifccParser::ParamscallContext *ctx);
		void readParams(vector<Parametre*> * parametres, ifccParser::ParamsContext *ctx);
		bool containsFunctionBB(string functionName);
};


