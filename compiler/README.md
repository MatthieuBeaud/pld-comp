# Manuel de Développeur

## Instructions

Pour le langage supporté par le compilateur, voir le manuel d'utilisation à la racine du repository.


## Compilation scripts
- `Makefile` can be used to compile the parser. Libraries and include directories default to the values that work in the IF machines of rooms 208 and 219.
- `compile_ubuntu.sh` is a script that compiles the project in a different environment (a ubuntu where the antlr runtime had to be compiled manually). Works well on WSL with ubuntu 20.04.
- `compile_docker.sh` is a script to use if you are desperate. It compiles the project in a docker environment where antlr4 and its dependencies are properly installed. 


## Source Files
- `ifcc.g4` contient la grammaire du langage compilable.
- `main.cpp` contient le code c++ pour appeler le parser antlr4 pour le fichier donné en ligne de command. Gère également la prise en compte de l'option ARM ou non pour générer l'assembleur en sortie.
- `visitor.h/cpp` Contiennent la classe `Visitor`.
- `IR.h/cpp` sont les fichiers qui permettent de gérer la représentation intermédiaire. Elle contient la classe du `CFG` (controle flow graph) , la classe du `BasicBlock`  , et toutes les classes des instructions `IRInstr`.
- `TableSymbole.h/cpp` contiennent la classe TableSymbole.
- `MultiVar.h/cpp` contiennent la classe Multivar.




## Structure des données


### Classe CFG

Chaque instance de cette classe correspond à une fonction du code. Les CFG font partie de la représentation intermédiaire.

### Classe BasicBlock

Chaque instance de BasicBlock correspond au contenu d'une paire de crochets. Plus clairement, elle permet de gérer les alternatives, les jumps, les contextes. Les BasicBlocks font partie de la représentation intermédiaire.

Les CFG ont comme attribut une liste de CFG.

### Classe IRInsrt

La classe IRinstre fait partie de la représentation intermédiaire, elle représente une petite section de code qui effectuent une tache simple. Cette classe est héritée pour chaque action spécifique à executer. Elle peut génerer du code assembleur en x86 ou ARM.

Chaque classe reçoit à sa création une liste de paramètres spécifique à l'action qu'elle doit effectuer.

Liste des Classes filles

- __IRInstrDiv__  : effectue une division
- __IRInstrMod__ : effectue un module
- __IRInstrNeg__ : effectue une negation
- __IRInstrLdconst__ : stock une constante dans une variable
- __IRInstrCopy__ : Copie une variable à une adresse mémoire
- __IRInstrAdd__ : effectue une addition
- __IRInstrSub__ : effectue une soustraction
- __IRInstrMul__ : effectue une multiplication
- __IRInstrRmem__ : Lit une case mémoire
- __IRInstrWmem__ : écrit dans une case mémoire
- __IRInstrCall__ : appelle une fonction
- __IRInstrCmpEq__ : test si deux entiers sont égaux
- __IRInstrCmpNeq__ : test si deux entiers sont différents
- __IRInstrCmplt__ : test l'infériorité entre deux entiers
- __IRInstrCmple__ : test la supériorité entre deux entiers
- __IRInstrOr__ : effectue un ou logique
- __IRInstrAnd__ : effectue un et logique
- __IRInstrXor__ : effectue un ou exclusif logique
- __IRInstrRet__ : termine le code d'une fonction
- __IRInstrInit_rbp__ : initialise le code d'une fonction


### Classe visitor

La classe visitor permet de visiter l'arbre issu du parsing, d'ajouter les instructions à la représentation intermédiaire, et de détecter des erreurs dans le code. Cette classe contient également une liste des fonctions déclarées avec leur type de retour et le type des paramètres attendus.


### Classe TableSymboles
Cette classe nous sert d'interface entre la mémoire et le programme. Elle permet pour chaque varibale, de stocker le nom, le type et la place dans la mémoire. Cette classe sert aussi à stocker, par le même procédé que pour les variables, le nom des fonctions, leur type de retour et les paramètres qu'elles nécéssitent. 

Les TableSymboles sont stockées dans les basicBlocs afin d'assurer une cohérence dans la portée de chaque varibale. 

Une TableSymboles est stockée dans la classe Visitor afin de stocker l'ensemble des fonctions existant dans le corps du programme.


### Classe Multivar

La classe multivar sert à stocker une liste de variables. elle est nottament utilisée pour la déclaration de plusieurs variables, la déclaration des fonctions et l'appel des fonctions.



