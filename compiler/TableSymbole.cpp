#include "TableSymbole.h"
#include <iostream>

TableSymbole::TableSymbole(){
    variables = map<string, Variable*>();
    offset = 0;
}


TableSymbole::TableSymbole(const  TableSymbole & tableCopie)
{
    variables = map<string,Variable*>();
    for(auto it=tableCopie.variables.begin(); it!=tableCopie.variables.end(); ++it)
    {
        variables.insert(pair<string, Variable*>(it->first, new Variable(*(it->second)))); //copie profonde
    }

    offset = tableCopie.GetOffset();
}

TableSymbole& TableSymbole::operator=(TableSymbole const& tableCopie)
{
    variables = map<string,Variable*>();
    for(auto it=tableCopie.variables.begin(); it!=tableCopie.variables.end(); ++it)
    {
        variables.insert(pair<string, Variable*>(it->first, new Variable(*(it->second)))); //copie profonde
    }

    offset = tableCopie.GetOffset();
    return *this;
}
  
TableSymbole::~TableSymbole()
{
    for(auto it= variables.begin(); it!=variables.end(); it++)
    {
        delete(it->second); //free profond
    }
}

void TableSymbole::AjouterSymbole(string nom, Type type) //déclaration
{   
    map<string,Variable*>::iterator it;

    it = variables.find(nom);
    if(it != variables.end()){
        cerr <<" double définition de la variable : " << nom << endl;
        exit(-1);
    }
    offset = offset-8;
    Variable* var = new Variable();
    var->type = type;
    var->offset = offset;
    var->used = false;
    variables.insert(pair<string, Variable*>(nom, var));
}

string TableSymbole::AjouterSymbole(Type type){
    offset -= 8;
    Variable* var = new Variable();
    var->type = type;
    var->offset = offset;
    var->used = false;
    string nomVariable = "tmp"+ std::to_string(offset/(-8)); //nom de variable temporelle (par exemple tmp2, tmp3, etc.)
    variables.insert(pair<string, Variable*>(nomVariable, var));
    return nomVariable;
}

Variable* TableSymbole::GetSymbole(string nom) 
{
    map<string,Variable*>::iterator it;
    Variable* var;
    it = variables.find(nom);
    if(it!= variables.end()){
        var = it->second;
        var->used = true;
        return var;
    }
    else{
        cerr <<"ERREUR: variable " << nom << " non définie "<< endl;
        exit(-1);
        return nullptr;
    }
}


void TableSymbole::AddFunction(string nom, ReturnType retType, vector<Type> params)
{
    Function* func = new Function();
    func->returnType = retType;
    func->params = vector<Type>(params);
    this->functions.insert(pair<string,Function*>(nom,func));
}

Function* TableSymbole::GetFunction(string nom)
{
    map<string,Function*>::iterator it;
    Function* func;
    it = functions.find(nom);
    if(it!= functions.end()){
        func = it->second;
        return func;
    }
    else{
        cerr <<"ERREUR: function " << nom << " non définie "<< endl;
        exit(-1);
        return nullptr;
    }
}



map<string, Variable*> TableSymbole::GetVariablesCopie() const {
    map<string, Variable*> copiesDeVariables = map<string,Variable*>();
    for(auto it=variables.begin(); it!=variables.end(); ++it)
    {
        copiesDeVariables.insert(pair<string, Variable*>(it->first, new Variable(*(it->second)))); //copie profonde
    }
    return variables;
}

void TableSymbole::ShowSymboles()
{
    for(auto it= variables.begin(); it!=variables.end(); it++)
    {
        cout<<it->first<<" / offset: "<<it->second->offset;
        cout<<" type: ";
        Type t = it->second->type;
        switch(t)
        {
            case Type::INT:
                cout<<"INT";
                break;
            case Type::CHAR:
                cout<<"CHAR";
                break;
        }
        cout<<" used: ";
        if(it->second->used)
        {
            cout<<"true";
        }else
        {
            cout<<"false";
        }
        cout<<endl;
    }
}