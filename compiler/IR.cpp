#include "IR.h"

//CFG-------------------------------------------------------

using namespace std;

int CFG::nextBBnumber = 0;

CFG::CFG()
{
	bbs = vector<BasicBlock*>();
}


string CFG::create_new_tempvar(Type t)
{
    return current_bb->tableSymbole->AjouterSymbole(t);
}

void CFG::add_to_symbol_table(string name, Type t)
{
    current_bb->tableSymbole->AjouterSymbole(name, t);
}

Variable * CFG::get_var(string name)
{
    return current_bb->tableSymbole->GetSymbole(name);
}


string CFG::new_BB_name()
{
	++(CFG::nextBBnumber);
    string fullName = ".L" + to_string(CFG::nextBBnumber);
    return fullName;
}

void CFG::gen_asm(ostream& o, OutputType t)
{
	gen_asm_prologue(o,t);
    for (vector<BasicBlock*>::iterator it = bbs.begin() ; it != bbs.end(); ++it)
    {
        (*it)->gen_asm(o,t);
    }
	gen_asm_epilogue(o,t);
}

void CFG::gen_asm_prologue(ostream& o, OutputType t)
{

}

void CFG::gen_asm_epilogue(ostream& o, OutputType t)
{

}

string CFG::IR_reg_to_asm(string varName, OutputType t)
{
	Variable* variable = current_bb->tableSymbole->GetSymbole(varName);
	string output_asm;
	switch(t)
	{
		case OutputType::x86:
			output_asm = to_string(variable->offset)+"(%rbp)";
			break;
		case OutputType::ARM:
			int nbVarInTabSymb = current_bb->tableSymbole->getNbVar();
			nbVarInTabSymb = (nbVarInTabSymb%16==0)?nbVarInTabSymb:nbVarInTabSymb+16;
			output_asm = "[sp, "+to_string(variable->offset+nbVarInTabSymb)+"]";
			break;
	}
	return output_asm;
}

void CFG::add_bb(BasicBlock* bb)
{
	current_bb=bb;
	bbs.push_back(bb);
}

bool CFG::containsBBWithLabel(string labelName)
{
	for(int i=0; i<bbs.size(); i++)
	{
		if(bbs[i]->label == labelName)
		{
			return true;
		}
	}
	return false;
}


// BasicBlock------------------------------------------------------------------------------------------

BasicBlock::BasicBlock(CFG* cfg, string entry_label){

	instrs = vector<IRInstr*>();
	tableSymbole = new TableSymbole();
	this->cfg = cfg;
	this->label = entry_label;
	exit_false = nullptr;
	exit_true = nullptr;
}

BasicBlock::BasicBlock(CFG* cfg, string entry_label, TableSymbole tableCopie){

	instrs = vector<IRInstr*>();
	tableSymbole = new TableSymbole(tableCopie);
	this->cfg = cfg;
	this->label = entry_label;
	exit_false = nullptr;
	exit_true = nullptr;
}

void BasicBlock::add_IRInstr(IRInstr::Operation op, Type t, vector<string> params){
	IRInstr* instr = new IRInstr(this, op, t, params);
	this->instrs.push_back(instr);
}

void BasicBlock::gen_asm(ostream &o, OutputType t){
	
	o<<this->label<<":"<<endl;
	for(std::vector<IRInstr*>::iterator it = this->instrs.begin(); it != this->instrs.end(); ++it)
	{
		(*it)->gen_asm(o, t); //on génère l'assembleur de chaque instruction individuelle
	}
	switch(t)
	{
		case OutputType::x86:

			if(exit_false!=nullptr){ 
				o << "\tcmpq $0, " << this->tableSymbole->GetSymbole(this->test_var_name)->offset<<"(%rbp)"<< endl;
				o << "\tje " << exit_false->label << endl;
				o << "\tjmp " <<exit_true->label << endl;
			}else if(exit_true!=nullptr){
				o << "\tjmp " <<exit_true->label << endl;
			}else{
				//jump vers l'épilogue
			}
			break;
		case OutputType::ARM:

			break;
	}
   
}



//IRInstr----------------------------------------------------------------------------------------------

IRInstr::IRInstr(BasicBlock* bb_, Operation op, Type t, vector<string> params){
	this->bb = bb_;
	this->op = op;
	this->t = t;
	this->params = params;
}

IRInstr::~IRInstr(){
}


//IRInstrLdconst
IRInstrLdconst::IRInstrLdconst(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::ldconst, t, params)
{}

void IRInstrLdconst::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrLdconst--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<< "\tmovq $"<<params[1]<<", "<<params[0]<<endl;
			break;
		case OutputType::ARM:
			o<< "\tmov x0, "<<params[1]<<endl;
			o<< "\tstr x0, "<<params[0]<<endl;
			break;
	}
}


//IRInstrCopy
IRInstrCopy::IRInstrCopy(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::copy, t, params)
{}

void IRInstrCopy::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrCopy--"<<endl;
	switch(t)
		{
			case OutputType::x86:
				o<<"\tmovq "<<params[1]<<", %rax"<<endl; 
				o<<"\tmovq %rax, "<<params[0]<<endl;
				break;
			case OutputType::ARM:
				o<<"\tldr x0"<<params[1]<<endl; 
				o<<"\tstr "<<params[0]<<", x0"<<endl;
				break;
		}
}


//IRInstrAdd
IRInstrAdd::IRInstrAdd(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::add, t, params)
{}

void IRInstrAdd::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrAdd--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[1]<<", %rax"<<endl; //on met la valeur de gauche de l'expression dans le total
			o<<"\tmovq %rax, "<<params[0]<<endl;
			o<<"\tmovq "<<params[2]<<", %rax"<<endl; // on ajoute la valeur de droite dans total
			o<<"\taddq %rax, "<<params[0]<<endl;
			break;
		case OutputType::ARM:
			o<<"\tmov x0, "<<params[1]<<endl; //on met la valeur de gauche dans le registre x0
			o<<"\tmov x1, "<<params[2]<<endl; //on met la valeur de droite dans le registre x1
			o<<"\tadd x0, x1 ,x0"<<endl;      //on met le résultat de l'addition dans le registre x0
			o<<"\tstr x0, "<<params[0]<<endl; //on stocke le résultat dans params[0]
			break;
	} 
}


//IRInstrSub
IRInstrSub::IRInstrSub(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::sub, t, params)
{}

void IRInstrSub::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrSub--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[1]<<", %rax"<<endl; //on met la valeur de gauche de l'expression dans le total
			o<<"\tmovq %rax, "<<params[0]<<endl;
			o<<"\tmovq "<<params[2]<<", %rax"<<endl; // on ajoute la valeur de droite dans total
			o<<"\tsubq %rax, "<<params[0]<<endl; 
			break;
		case OutputType::ARM:
			o<<"\tmov x0, "<<params[1]<<endl; //on met la valeur de gauche dans le registre x0
			o<<"\tmov x1, "<<params[2]<<endl; //on met la valeur de droite dans le registre x1
			o<<"\tsub x0, x1 ,x0"<<endl;      //on met le résultat de la soustraction dans le registre x0
			o<<"\tstr x0, "<<params[0]<<endl; //on stocke le résultat dans params[0]
			break;
	} 
}


//IRInstrMul
IRInstrMul::IRInstrMul(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::mul, t, params)
{}

void IRInstrMul::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrMul--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[1]<<", %rax"<<endl; 
			o<<"\tmulq "<<params[2]<<endl;
			o<<"\tmovq %rax, "<<params[0]<<endl;	//on met la valeur de rax dans la variable du total
			break;
		case OutputType::ARM:
			o<<"\tmov x0, "<<params[1]<<endl; //on met la valeur de gauche dans le registre x0
			o<<"\tmov x1, "<<params[2]<<endl; //on met la valeur de droite dans le registre x1
			o<<"\tmul x0, x1 ,x0"<<endl;      //on met le résultat de la multiplication dans le registre x0
			o<<"\tstr x0, "<<params[0]<<endl; //on stocke le résultat dans params[0]
			break;
	} 
}


//IRInstrRmem
IRInstrRmem::IRInstrRmem(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::rmem, t, params)
{}

void IRInstrRmem::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrRmem--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[1]<<", %rax"<<endl; 
			o<<"\tmovq %rax, "<<params[0]<<endl;
			break;
		case OutputType::ARM:
			o<<"\tldr x0"<<params[1]<<endl; 
			o<<"\tstr "<<params[0]<<", x0"<<endl;
			break;	
	} 
}


//IRInstrWmem
IRInstrWmem::IRInstrWmem(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::wmem, t, params)
{}

void IRInstrWmem::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrWmem--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[0]<<", %rax"<<endl;
			o<<"\tmovq %rax, "<<params[1]<<endl;
			break;
		case OutputType::ARM:
			o<<"\tldr x0"<<params[0]<<endl; 
			o<<"\tstr "<<params[1]<<", x0"<<endl;
			break;
	} 
}


//IRInstrCall
IRInstrCall::IRInstrCall(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::call, t, params)
{}

void IRInstrCall::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrCall--"<<endl;
	int nbParam = params.size()-2;
	string registres[6] = { "%rdi" ,"%rsi", "%rdx", "%rcx","%r8","%r9"};
	int cpt = nbParam;
	switch(t)
	{
		case OutputType::x86:
			for (int cpt = nbParam-1; cpt >=0 ;cpt--)
			{
				if(cpt>5)
				{
					o << "\tpushq " << params[cpt+2] <<endl;
				}
				else
				{
					o<< "\tmovq " <<params[cpt+2]<<", " << registres[cpt]<<endl;
				}
			} 
			o<<"\tcall  "<<params[1]<<endl;
			o<<"\tmovq %rax, "<<params[0]<<endl;
			break;
		case OutputType::ARM:
			break;
	} 
}


//IRInstrCmpEq
IRInstrCmpEq::IRInstrCmpEq(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::cmp_eq, t, params)
{}

void IRInstrCmpEq::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstreCmpEq--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<< "\tmovq "<<params[1]<<", "<<"%rax"<<endl;
			o<< "\tcmpq %rax, "<<params[2]<<endl;
			o<< "\tsete %al"<<endl; //on met le resultat dans le registre a
			o<< "\tmovzbl %al, %eax"<<endl; // on met des zeros dans tous les bits sauf le dernier (le 1 ou 0)
			o<< "\tmovq %rax, "<<params[0]<<endl; //on met le resultat dans dest
			break;
		case OutputType::ARM:
			o<< "\tldr x0, "<<params[1]<<endl;
			o<< "\tldr x1, "<<params[2]<<endl;
			o<< "\tcmp x0, x1"<<endl;
			o<< "\tite eq"<<endl;
			o<< "\tmoveq x2, #1"<<endl;
			o<< "\tmovne x2, #0"<<endl;
			o<< "\tuxtb x2, x2"<<endl;
			o<< "\tstr x2, "<<params[0]<<endl;
			break;
	} 
}


//IRInstrCmpNeq
IRInstrCmpNeq::IRInstrCmpNeq(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::cmp_eq, t, params)
{}

void IRInstrCmpNeq::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstreCmpNeq--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<< "\tmovq "<<params[1]<< ", "<< "%rax"<<endl;
			o<< "\tcmpq %rax, "<<params[2]<<endl;
			o<< "\tsetne %al"<<endl; //on met le resultat dans le registre a
			o<< "\tmovzbl %al, %eax"<<endl; // on met des zeros dans tous les bits sauf le dernier (le 1 ou 0)
			o<< "\tmovq %rax, "<<params[0]<<endl; //on met le resultat dans dest
			break;
		case OutputType::ARM:
			o<< "\tldr x0, "<<params[1]<<endl;
			o<< "\tldr x1, "<<params[2]<<endl;
			o<< "\tcmp x0, x1"<<endl;
			o<< "\tite ne"<<endl;
			o<< "\tmovne x2, #1"<<endl;
			o<< "\tmoveq x2, #0"<<endl;
			o<< "\tuxtb x2, x2"<<endl;
			o<< "\tstr x2, "<<params[0]<<endl;
			break;
	} 
}


//IRInstrCmplt
IRInstrCmplt::IRInstrCmplt(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::cmp_lt, t, params)
{}

void IRInstrCmplt::gen_asm(ostream &o, OutputType t)
{
 	o<<"#--IRInstreCmplt--"<<endl;
	 switch(t)
	{
		case OutputType::x86:
			o<< "\tmovq "<<params[1]<<", "<<"%rax"<<endl;
			o<< "\tcmpq %rax, "<<params[2]<<endl;
			o<< "\tsetl %al"<<endl; //on met le resultat dans le registre a
			o<< "\tmovzbl %al, %eax"<<endl; // on met des zeros dans tous les bits sauf le dernier (le 1 ou 0)
			o<< "\tmovq %rax, "<<params[0]<<endl; //on met le resultat dans dest
			break;
		case OutputType::ARM:
			o<< "\tldr x0, "<<params[1]<<endl;
			o<< "\tldr x1, "<<params[2]<<endl;
			o<< "\tcmp x0, x1"<<endl;
			o<< "\tite lt"<<endl;
			o<< "\tmovlt x2, #1"<<endl;
			o<< "\tmovge x2, #0"<<endl;
			o<< "\tuxtb x2, x2"<<endl;
			o<< "\tstr x2, "<<params[0]<<endl;
			break;
	} 
}


//IRInstrCmple
IRInstrCmple::IRInstrCmple(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::cmp_le, t, params)
{}

void IRInstrCmple::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstreCmple--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<< "\tmovq " << params[1] << ", "<<"%rax"<<endl;
			o<< "\tcmpq %rax, "<<params[2]<<endl;
			o<< "\tsetle %al"<<endl; //on met le resultat dans le registre a
			o<< "\tmovzbl %al, %eax"<<endl; // on met des zeros dans tous les bits sauf le dernier (le 1 ou 0)
			o<< "\tmovq %rax, "<<params[0]<<endl; //on met le resultat dans dest
			break;
		case OutputType::ARM:
			o<< "\tldr x0, "<<params[1]<<endl;
			o<< "\tldr x1, "<<params[2]<<endl;
			o<< "\tcmp x0, x1"<<endl;
			o<< "\tite le"<<endl;
			o<< "\tmovle x2, #1"<<endl;
			o<< "\tmovgt x2, #0"<<endl;
			o<< "\tuxtb x2, x2"<<endl;
			o<< "\tstr x2, "<<params[0]<<endl;
			break;
	} 
}


//IRInstrAnd
IRInstrAnd::IRInstrAnd(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::op_and, t, params)
{}

void IRInstrAnd::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstreAnd--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[1]<<", %rax"<<endl; 
			//on met la valeur de gauche de l'expression dans le registre arithmétique
			o<<"\tandq "<<params[0]<<", %rax"<<endl; 
			// on compare (le et bit à bit) la valeur de droite par celle contenue dans le registre arithmétique et on la stock dans le registre arithmétique
			o<<"\tmovq %rax, "<<params[2]<<endl;
			//on met la valeur de rax dans la variable du total
			break;
		case OutputType::ARM:
			o<<"\tmov x0, "<<params[1]<<endl; //on met la valeur de gauche dans le registre x0
			o<<"\tmov x1, "<<params[2]<<endl; //on met la valeur de droite dans le registre x1
			o<<"\tand x0, x1 ,x0"<<endl;      //on met le résultat du "et" dans le registre x0
			o<<"\tstr x0, "<<params[0]<<endl; //on stocke le résultat dans params[0]
			break;
	} 
}


//IRInstrOr
IRInstrOr::IRInstrOr(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::op_or, t, params)
{}

void IRInstrOr::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstreOu--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[1]<<", %rax"<<endl; //on met la valeur de gauche de l'expression dans le registre arithmétique
			o<<"\torq "<<params[0]<<", %rax"<<endl; // on compare (le ou bit à bit) la valeur de droite par celle contenue dans le registre arithmétique
			o<<"\tmovq %rax, "<<params[2]<<endl;	//on met la valeur de rax dans la variable du total
			break;
		case OutputType::ARM:
			o<<"\tmov x0, "<<params[1]<<endl; //on met la valeur de gauche dans le registre x0
			o<<"\tmov x1, "<<params[2]<<endl; //on met la valeur de droite dans le registre x1
			o<<"\torr x0, x1 ,x0"<<endl;      //on met le résultat du "ou" dans le registre x0
			o<<"\tstr x0, "<<params[0]<<endl; //on stocke le résultat dans params[0]
			break;
	} 
}


//IRInstrXor
IRInstrXor::IRInstrXor(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::op_xor, t, params)
{}

void IRInstrXor::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstreOux--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[1]<<", %rax"<<endl; //on met la valeur de gauche de l'expression dans le registre arithmétique
			o<<"\txorq "<<params[0]<<", %rax"<<endl; // on compare (le oux bit à bit) la valeur de droite par celle contenue dans le registre arithmétique
			o<<"\tmovq %rax, "<<params[2]<<endl;	//on met la valeur de rax dans la variable du total
			break;
		case OutputType::ARM:
			o<<"\tmov x0, "<<params[1]<<endl; //on met la valeur de gauche dans le registre x0
			o<<"\tmov x1, "<<params[2]<<endl; //on met la valeur de droite dans le registre x1
			o<<"\teor x0, x1 ,x0"<<endl;      //on met le résultat du "ou exclusif" dans le registre x0
			o<<"\tstr x0, "<<params[0]<<endl; //on stocke le résultat dans params[0]
			break;
	} 
}


//IrInstrRet
IRInstrRet::IRInstrRet(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::ret, t, params)
{}

void IRInstrRet::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstreRet--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			if(params.size()>1)
			{
				o<<"\tmovq "<<params[1]<<", %rax"<<endl;
			}
			o<<"\tleave            # restore %rbp from the stack"<<endl;
			o<<"\tret              # return to the caller"<<endl;
			break;
		case OutputType::ARM:
			if(params.size()>1)
			{
				o<<"\tldr x0, "<<params[1]<<endl;
			}
			o<<"\tadd sp, sp, "<<params[0]<<endl;
			o<<"\tret              # return to the caller"<<endl;
			break;
	} 
}


//IRInstrInit_rbp
IRInstrInit_rbp::IRInstrInit_rbp(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::ret, t, params)
{}

void IRInstrInit_rbp::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrInit_rbp--"<<endl;
	int nbvar = this->bb->cfg->getNbVars();
	string registres[6] = { "%rdi" ,"%rsi", "%rdx", "%rcx","%r8","%r9"};
	vector<string>::iterator it;
	int cpt = 0;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tpushq %rbp          # save %rbp on the stack"<<endl;
			o<<"\tmovq %rsp, %rbp      # define %rbp for the current function"<<endl;
			if(nbvar !=0)
			{
				o<< "\tsubq $"<< to_string(nbvar*8)<<", %rsp"<<endl;
			}
			for(it=params.begin(); it!=params.end(); ++it)
			{
				if(cpt< 6)
				{
					o<<"\tmovq "<< registres[cpt]<< ", "<<*it<<endl;
				}
				else
				{
					string offsetSansRbp = it->substr(0, it->find_first_of('('));	// *it contains "offset(%rbp)" so we remove the (%rbp) to get only the offset to stoi
					o<<"\tmovq "<< to_string(stoi(offsetSansRbp)+8*(params.size()+2)) << "(%rbp), %rax"<< endl;
					o<<"\tmovq %rax, "<<*it<< endl;
				}
				cpt++;
			}
			break;
		case OutputType::ARM:
			o<<"\tsub sp, sp, #"<<to_string(nbvar*8)<<endl;
			break;
	} 
}


//IRInstrNot
IRInstrNot::IRInstrNot(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::make_not, t, params)
{}

void IRInstrNot::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrNot--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[0]<<", %rax"<<endl; 
			//on met la valeur de gauche de l'expression dans le registre arithmétique
			o<<"\tcmpq $0, %rax"<<endl;
			o<<"\tsete %al"<<endl;
			o<< "\tmovzbl %al, %eax"<<endl;
			o<< "\tmovq %rax, "<<params[1]<<endl; //on met le resultat dans dest
			break;
		case OutputType::ARM:
			o<< "\tldr x0, "<<params[1]<<endl;
			o<< "\tcmp x0, #0"<<endl;
			o<< "\tite eq"<<endl;
			o<< "\tmoveq x2, #1"<<endl;
			o<< "\tmovne x2, #0"<<endl;
			o<< "\tuxtb x2, x2"<<endl;
			o<< "\tstr x2, "<<params[0]<<endl;
			break;
	} 
}


//IRIntrNeg
IRInstrNeg::IRInstrNeg(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::make_neg, t, params)
{}

void IRInstrNeg::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrNeg--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[0]<<", %rax"<<endl; 
			//on met la valeur de gauche de l'expression dans le registre arithmétique
			o<<"\tnegq "<< "%rax"<<endl; 
			// On remplace %rax par son opposé dans %rax
			o<<"\tmovq %rax, "<<params[1]<<endl;
			//on met la valeur de rax dans la variable du total
			break;
		case OutputType::ARM:
			o<<"\tldr x0, "<<params[0]<<endl;
			o<<"\tneg x0, x0"<<endl;
			o<<"\tstr x0, "<<params[1]<<endl; 
			break;
	} 
}


//IRIntrDiv
IRInstrDiv::IRInstrDiv(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::make_neg, t, params)
{}

void IRInstrDiv::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrDiv--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[0]<<", %rax"<<endl; //on met la valeur de gauche de l'expression dans le registre arithmétique
			o<<"\tmovq $0, %rdx"<<endl;	//rdx contient les 32 bits les plus grands du diviseur
			o<<"\tdivq "<<params[1]<<endl; // on divise la valeur de droite par celle contenue dans le registre arithmétique
			o<<"\tmovq %rax, "<<params[2]<<endl;	//on met la valeur de rax dans la variable du total
			break;
		case OutputType::ARM:
			o<<"\tmov x0, "<<params[1]<<endl; //on met la valeur de gauche dans le registre x0
			o<<"\tmov x1, "<<params[2]<<endl; //on met la valeur de droite dans le registre x1
			o<<"\tsdiv x0, x1 ,x0"<<endl;      //on met le résultat de la division dans le registre x0
			o<<"\tstr x0, "<<params[0]<<endl; //on stocke le résultat dans params[0]
			break;
	} 
}

//IRIntrModulo

IRInstrMod::IRInstrMod(BasicBlock* bb, Type t, vector<string> params)
: IRInstr(bb, IRInstr::make_neg, t, params)
{}

void IRInstrMod::gen_asm(ostream &o, OutputType t)
{
	o<<"#--IRInstrMod--"<<endl;
	switch(t)
	{
		case OutputType::x86:
			o<<"\tmovq "<<params[0]<<", %rax"<<endl; //on met la valeur de gauche de l'expression dans le registre arithmétique
			o<<"\tmovq $0, %rdx"<<endl;	//rdx contient les 32 bits les plus grands du diviseur
			o<<"\tdivq "<<params[1]<<endl; // on divise la valeur de droite par celle contenue dans le registre arithmétique
			o<<"\tmovq %rdx, "<<params[2]<<endl;	//on met la valeur de rdx dans la variable du total
			break;
		case OutputType::ARM:
			o<<"\tmov x0, "<<params[1]<<endl; //on met la valeur de gauche dans le registre x0
			o<<"\tmov x1, "<<params[2]<<endl; //on met la valeur de droite dans le registre x1
			o<<"\tsdiv x0, x1 ,x0"<<endl;      //on met le résultat de la division dans le registre x0
			o<<"\tstr x0, "<<params[0]<<endl; //on stocke le résultat dans params[0]
			break;
	} 
}